import unittest

from num_decode_variants import decode 


class TestDecode(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = decode('111')
		self.assertEqual(res, 3)

	def test_small(self):
		res1 = decode('10')
		res2 = decode('11')
		res3 = decode('27')

		self.assertEqual(res1, 1)
		self.assertEqual(res2, 2)
		self.assertEqual(res3, 1)

	def test_weird_long(self):
		s = '3834759837495845834759873486754954'
		res = decode(s)
		self.assertEqual(res, 1)

	def test_other(self):
		s = '312348'
		res = decode(s)
		self.assertEqual(res, 3)


if __name__ == '__main__':
	unittest.main()