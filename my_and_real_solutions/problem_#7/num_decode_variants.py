"""
Asked by Facebook.

Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, 
count the number of ways it can be decoded.

For example, the message '111' would give 3, 
since it could be decoded as 'aaa', 'ka', and 'ak'.

You can assume that the messages are decodable.
For example, '001' is not allowed.
"""


def decode(s):
	count = 0

	if len(s) == 1 and s[0] == '0':
		return 0

	elif len(s) <= 1:
		return count + 1


	count += decode(s[1:])

	if int(s[:2]) < 27:
		count += decode(s[2:])

	return count
	