"""
This problem was asked by Facebook.

Given a list of integers, return the largest product that can be made by multiplying any three integers.

For example, if the list is [-10, -10, 5, 2], we should return 500, since that's -10 * -10 * 5.

You can assume the list has at least three integers.
"""


def three_smallest(lst):
    min1 = min(lst)
    lst.remove(min1)
    min2 = min(lst)
    lst.remove(min2)
    min3 = min(lst)
    return min1, min2, min3


def three_largest(lst):
    max1 = max(lst)
    lst.remove(max1)
    max2 = max(lst)
    lst.remove(max2)
    max3 = max(lst)
    return max1, max2, max3


def max_product(lst):
    temp = lst.copy()
    min1, min2, min3 = three_smallest(temp)
    temp = lst.copy()
    max1, max2, max3 = three_largest(temp)

    return max(max1 * min1 * min2, max1 * max2 * max3)

