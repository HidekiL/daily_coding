import random 
import unittest

from max_product_of_three import max_product as f 
from sol import maximum_product_of_three as _


class TestMaxProduct(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([-10, -10, 5, 2])
		self.assertEqual(res , 500)

	def test_my_example(self):
		res = f([-1, -2, -8, 14, 1])
		self.assertEqual(res, 224)

		res = f([-3, -5, 4, 7])
		self.assertEqual(105, res)

	def test_empty(self):
		pass
		
	def test_small(self):
		res = f([2, 3, 4])
		self.assertEqual(res, 24)

	def test_weird(self):
		res = f([-1, -1, -2, 0, 0, 1, 1, 1])
		self.assertEqual(res, 2)

	def test_big(self):
		pass

	def test_random(self):
		inp = [random.randint(-100, 100) for i in range(10)]
		self.assertEqual(f(inp), _(inp))


if __name__ == '__main__':
	unittest.main()