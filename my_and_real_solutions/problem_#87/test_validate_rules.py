import random
import unittest

from validate_rules import validate as f


class TestValidate(unittest.TestCase):
    def setUp(self):
        pass

    def test_example(self):
        res = f(["A N B",
                 "B NE C",
                 "C N A"])
        self.assertEqual(res, False)

        res = f(["A NW B",
                 "A N B"])
        self.assertEqual(res, True)

    def test_my_example(self):
        res = f(["A N B",
                 "B NE C",
                 "C S A"])
        self.assertEqual(res, True)

    def test_empty(self):
        pass

    def test_small(self):
        pass

    def test_weird(self):
        pass

    def test_big(self):
        pass

    def test_random(self):
        pass


if __name__ == '__main__':
    unittest.main()
