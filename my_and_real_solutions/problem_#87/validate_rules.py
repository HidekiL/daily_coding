"""
This problem was asked by Uber.

A rule looks like this:

A NE B

This means this means point A is located northeast of point B.

A SW C

means that point A is southwest of C.

Given a list of rules, check if the sum of the rules validate. For example:

A N B
B NE C
C N A
does not validate, since A cannot be both north and south of C.

A NW B
A N B
is considered valid.
"""


DIRECTIONS = {'N': (-1, 0), 'W': (0, -1), 'E': (0, 1), 'S': (1, 0)}


def give_location(first, directions):
	for direction in directions:
		first = int(first[0]) + DIRECTIONS[direction][0], int(first[1]) + DIRECTIONS[direction][1]
	return first


def check_validity(first, second, directions):
	for direction in directions:
		if direction == 'N':
			if first[0] <= second[0]:
				return False
		if direction == 'S':
			if first[0] >= second[0]:
				return False
		if direction == 'W':
			if first[1] <= second[1]:
				return False
		if direction == 'E':
			if first[1] >= second[1]:
				return False

	return True


def validate(rules):
	map_dict = {}
	first, directions, second = rules[0].split(' ')
	map_dict[first] = (0, 0)
	map_dict[second] = give_location(map_dict[first], directions)
	valid = True

	for i in range(1, len(rules)):
		first, directions, second = rules[i].split(' ')
		if first in map_dict and second in map_dict:
			valid = check_validity(map_dict[first], map_dict[second], directions)
			if not valid:
				return False
		else:
			map_dict[second] = give_location(map_dict[first], directions)

	return valid


print(validate(["A N B",
                "B NE C",
                "C N A"]))
