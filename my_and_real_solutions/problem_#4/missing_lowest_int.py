"""
Asked by Stripe

Given an array of integers, 
find the first missing positive integer in linear time and constant space. 
In other words, find the lowest positive integer that does not exist in the array. 
The array can contain duplicates and negative numbers as well.

For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.

You can modify the input array in-place.
"""


def missed(ls):
	if len(ls) == 0:
		return None

	ls = set(ls)
	max_num = max(ls)

	for i in range(1, max_num):
		if i not in ls:
			return i

	return 1