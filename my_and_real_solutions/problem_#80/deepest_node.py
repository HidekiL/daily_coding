"""
This problem was asked by Google.

Given the root of a binary tree, return a deepest node. For example, in the following tree, return d.

	  a
  	/  \
   b    c
  /
 d
"""


class Node:
	def __init__(self, val):
		self.val = val
		self.right = None
		self.left = None


def deepest_node(root):
	queue = [root]
	node = queue[0]

	while queue:
		node = queue.pop(0)

		if node.left:
			queue.append(node.left)

		if node.right:
			queue.append(node.right)

	return node.val


tree = Node('a')
tree.left = Node('b')
tree.left.left = Node('d')
tree.right = Node('c')

print(deepest_node(tree))
