"""
This problem was asked by Google.

Given k sorted singly linked lists, write a function to merge all the lists into one sorted singly linked list.
"""


class Node:
	def __init__(self, val):
		self.val = val
		self.next = None


class LinkedList:
	def __init__(self):
		self.head = None

	def insert(self, node):
		last = self.head

		while last.next:
			last = last.next
		last.next = node


def min_in_linked_lists(linked_lists):
	res, ind = linked_lists[0].val, 0

	for i in range(1, len(linked_lists)):
		if linked_lists[i].val < res:
			res, ind = linked_lists[i].val, i

	linked_lists[ind] = linked_lists[ind].next
	if linked_lists[ind] is None:
		del linked_lists[ind]

	return res


def merge_linked_lists(linked_lists):
	res_list = LinkedList()
	res_list.head = Node(min_in_linked_lists(linked_lists))

	while linked_lists:
		res_list.insert(Node(min_in_linked_lists(linked_lists)))

	return res_list


ll1 = LinkedList()
ll1.head = Node(1)
ll1.head.next = Node(3)
ll1.head.next.next = Node(5)

ll2 = LinkedList()
ll2.head = Node(2)
ll2.head.next = Node(9)
ll2.head.next.next = Node(10)

ll3 = LinkedList()
ll3.head = Node(0)
ll3.head.next = Node(11)
ll3.head.next.next = Node(19)

ll4 = LinkedList()
ll4.head = Node(-3)
ll4.head.next = Node(4)
ll4.head.next.next = Node(7)

merged_list = merge_linked_lists([ll1.head, ll2.head, ll3.head, ll4.head])

print(merged_list.head.val)
merged_list = merged_list.head.next

while merged_list:
	print(merged_list.val)
	merged_list = merged_list.next
