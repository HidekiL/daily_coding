import unittest

from pair import cons, cdr, car


class TestCons(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		self.assertEqual(car(cons(3,4)), 3)
		self.assertEqual(cdr(cons(3,4)), 4)


if __name__ == '__main__':
	unittest.main()