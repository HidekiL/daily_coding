import unittest
import random

from max_of_subarrays import max_of_subarrays as m 
from sol import max_of_subarrays as f


class TestMaxOfSubarrays(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = m([10, 5, 2, 7, 8, 7], 3)
		self.assertEqual(res, [10, 7, 8, 8])

	def test_empty(self):
		res = m([], 2)
		self.assertEqual(res, [])

	def test_zero_length(self):
		res = m([1, 2, 3, 4, 5], 0)
		self.assertEqual(res, [])

	def test_one_length(self):
		res = m([1, 2, 3, 4, 5], 1)
		self.assertEqual(res, [1, 2, 3, 4, 5])

	def test_other(self):
		arr = [[71, 95, 38, 100, 42, 82], [68, 85, 6, 60, 6, 66], [75, 98, 19, 44, 67, 35, 61, 71, 71, 91]]
		k = [2, 2, 4]
		self.assertEqual(m(arr[0], k[0]), [95, 95, 100, 100, 82])
		self.assertEqual(m(arr[1], k[0]), [85, 85, 60, 60, 66])
		self.assertEqual(m(arr[2], k[2]), [98, 98, 67, 67, 71, 71, 91])
		

	# Test after completing using function from real solution
	def test_big_random(self):
		arr = [random.randint(1, 10000) for _ in range(100)]
		k = random.randint(1, 10)
		self.assertEqual(m(arr, k), f(arr, k))


if __name__ == '__main__':
	unittest.main()