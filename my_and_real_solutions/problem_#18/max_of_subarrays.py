"""
This problem was asked by Google.

Given an array of integers and a number k, 
where 1 <= k <= length of the array, 
compute the maximum values of each subarray of length k.

For example, given array = [10, 5, 2, 7, 8, 7] and k = 3, 
we should get: [10, 7, 8, 8], since:

10 = max(10, 5, 2)
7 = max(5, 2, 7)
8 = max(2, 7, 8)
8 = max(7, 8, 7)
Do this in O(n) time and O(k) space. 
You can modify the input array in-place and you do not need to store the results. 
You can simply print them out as you compute them.
"""


def max_of_subarrays(arr, k):
	if not k or not arr:
		return []
		
	if k == 1:
		return arr

	curr_subarray = sorted(arr[:k])
	res = [curr_subarray[-1]]

	for ind in range(len(arr)-k):
		curr_subarray.remove(arr[ind])

		for i in range(len(curr_subarray)-1, -1, -1):
			if arr[ind+k] > curr_subarray[i]:
				curr_subarray.insert(i+1, arr[ind+k])
				break
			elif i == 0:
				curr_subarray.insert(0, arr[ind+k])

			
		res.append(curr_subarray[-1])

	return res
