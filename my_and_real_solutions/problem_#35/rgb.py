"""
This problem was asked by Google.

Given an array of strictly the characters 'R', 'G', and 'B', 
segregate the values of the array so that all the Rs come first, 
the Gs come second, and the Bs come last. You can only swap elements of the array.

Do this in linear time and in-place.

For example, given the array ['G', 'B', 'R', 'R', 'B', 'R', 'G']	, 
it should become ['R', 'R', 'R', 'G', 'G', 'B', 'B'].
"""


def sort_rgb(arr):
	if len(arr) <= 1:
		return arr

	last_b_ind = 0

	for i in range(len(arr)-1, -1, -1):
		if arr[i] == 'B':
			continue
		else:
			last_b_ind = i
			break

	for i in range(len(arr)):
		if i >= last_b_ind:
			break

		if arr[i] == 'B':
			arr[i], arr[last_b_ind] = arr[last_b_ind], arr[i]
			while arr[last_b_ind] == 'B':
				last_b_ind -= 1

	last_g_ind = 0

	for i in range(len(arr)-1, -1, -1):
		if arr[i] == 'G' or arr[i] == 'B':
			continue
		else:
			last_g_ind = i
			break

	for i in range(len(arr)):
		if i >= last_g_ind:
			break

		if arr[i] == 'G':
			arr[i], arr[last_g_ind] = arr[last_g_ind], arr[i]
			while arr[last_g_ind] == 'G':
				last_g_ind -= 1

	return arr
