import random 
import unittest

from rgb import sort_rgb as f 


class TestSort_rgb(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		inp = ['G', 'B', 'R', 'R', 'B', 'R', 'G']
		res = f(inp)
		self.assertEqual(res, sorted(inp, reverse=True))

	def test_my_example(self):
		pass
		
	def test_empty(self):
		res = f([])
		self.assertEqual(res, [])
		
	def test_small(self):
		res1 = f(['R', 'B'])
		self.assertEqual(res1, ['R', 'B'])

		res2 = f(['G', 'R'])
		self.assertEqual(res2, ['R', 'G'])

		res3 = f(['R'])
		self.assertEqual(res3, ['R'])

	def test_weird(self):
		inp = ['R'] * 10
		res = f(inp)
		self.assertEqual(res, inp)

	def test_big(self):
		pass

	def test_random(self):
		inp = []
		for i in range(100000):
			inp.append(random.choice(['R', 'G', 'B']))

		res = f(inp)
		self.assertEqual(res, sorted(inp, reverse=True))


if __name__ == '__main__':
	unittest.main()