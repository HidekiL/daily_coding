import unittest
import random

from min_rooms import min_rooms as f
from sol import max_overlapping as _ 


class TestMinRooms(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([(30, 75), (0, 50), (60, 150)])
		self.assertEqual(res, 2)

	def test_empty(self):
		res = f([])
		self.assertEqual(res, 0)

	def test_small(self):
		res1 = f([(0, 30), (30, 100)])
		res2 = f([(10, 60)])
		res3 = f([(25, 100), (0, 50)])
		self.assertEqual(res1, 1)
		self.assertEqual(res2, 1)
		self.assertEqual(res3, 2)

	def test_weird(self):
		res = f([(0, 10) for _ in range(100)])
		self.assertEqual(res, 100)

	def test_my_example(self):
		res = f([(10, 40), (100, 140), (30, 70), (25, 100), (150, 200), (0, 10)])
		self.assertEqual(res, 3)

	def test_random(self):
		inp = [(random.randint(1, 10000), random.randint(1, 100000)) for _ in range(1000)]
		self.assertEqual(f(inp), _(inp))
		

if __name__ == '__main__':
	unittest.main()