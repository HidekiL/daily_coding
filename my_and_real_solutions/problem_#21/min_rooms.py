"""
This problem was asked by Snapchat.

Given an array of time intervals (start, end) 
for classroom lectures (possibly overlapping), 
find the minimum number of rooms required.

For example, given [(30, 75), (0, 50), (60, 150)], 
you should return 2.
"""


import bisect


def min_rooms(time_intervals):
	time_intervals.sort()
	end_of_times = []
	res = 0

	for time_interval in time_intervals:
		if not end_of_times:
			res += 1
			end_of_times.append(time_interval[1])
			continue

		for i in range(len(end_of_times)-1, -1, -1):
			if time_interval[0] >= end_of_times[i]:
				del end_of_times[i]
				bisect.insort(end_of_times, time_interval[1])
				break

			elif i == 0:
				res += 1
				bisect.insort(end_of_times, time_interval[1])
				break

	return res
