"""
This problem was asked by Facebook.

Given a array of numbers representing the stock prices of a company in chronological order, 
write a function that calculates the maximum profit you could have made from buying and selling that stock once. You must buy before you can sell it.

For example, given [9, 11, 8, 5, 7, 10], you should return 5, 
since you could buy the stock at 5 dollars and sell it at 10 dollars.description goes here
"""


def max_profit(stock_prices):
	maxim_pr = 0
	min_stock = float('inf')

	for curr_stock in stock_prices:
		if curr_stock < min_stock:
			min_stock = curr_stock

		else:
			maxim_pr = max(maxim_pr, curr_stock - min_stock)

	return maxim_pr

