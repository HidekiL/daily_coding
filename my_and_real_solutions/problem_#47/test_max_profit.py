import random 
import unittest

from max_profit import max_profit as f 
from sol import buy_and_sell as _


class TestMaxProfit(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([9, 11, 8, 5, 7, 10])
		self.assertEqual(res, 5)

	def test_my_example(self):
		res = f([9, 11, 10, 5, 18, 11, 22])
		self.assertEqual(res, 17)
		
	def test_empty(self):
		res = f([])
		self.assertEqual(res, 0)
		
	def test_small(self):
		res = f([1, 1])
		self.assertEqual(res, 0)

	def test_weird(self):
		res = f([1]*10)
		self.assertEqual(res, 0)

		res = f([10, 9, 8, 5, 3, 2, 1])
		self.assertEqual(res, 0)

	def test_big(self):
		pass

	def test_random(self):
		inp = [random.randint(1, 100) for i in range(100)]
		self.assertEqual(f(inp), _(inp))


if __name__ == '__main__':
	unittest.main()