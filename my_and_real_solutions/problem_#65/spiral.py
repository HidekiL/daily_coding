"""
This problem was asked by Amazon.

Given a N by M matrix of numbers, print out the matrix in a clockwise spiral.

For example, given the following matrix:

[[1,  2,  3,  4,  5],
 [6,  7,  8,  9,  10],
 [11, 12, 13, 14, 15],
 [16, 17, 18, 19, 20]]
You should print out the following:

1
2
3
4
5
10
15
20
19
18
17
16
11
6
7
8
9
14
13
12
"""


def spiral(lst):
    first_row, last_row = 0, len(lst)
    first_col, last_col = 0, len(lst[0])

    iteration = 0
    while first_row != last_row and first_col != last_col:
        if iteration % 4 == 0:
            for col in range(first_col, last_col):
                print(lst[first_row][col])
            first_row += 1

        elif iteration % 4 == 1:
            for row in range(first_row, last_row):
                print(lst[row][last_col - 1])
            last_col -= 1

        elif iteration % 4 == 2:
            for col in range(last_col - 1, first_col - 1, -1):
                print(lst[last_row - 1][col])
            last_row -= 1

        else:
            for row in range(last_row - 1, first_row - 1, -1):
                print(lst[row][first_col])
            first_col += 1

        iteration += 1


spiral([[1, 2, 3, 4, 5],
        [6, 7, 8, 9, 10],
        [11, 12, 13, 14, 15],
        [16, 17, 18, 19, 20]])
