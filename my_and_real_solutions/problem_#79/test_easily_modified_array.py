import random 
import unittest

from easily_modified_array import can_easily_modified as f 
from sol import check as res_f


class TestCanEasilyModified(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([10, 5, 7])
		self.assertEqual(res, True)

		res = f([10, 5, 1])
		self.assertEqual(res, False)

	def test_my_example(self):
		res = f([10, 7, 5])
		self.assertEqual(res, False)
		
	def test_empty(self):
		res = f([])
		self.assertEqual(res, True)
		
	def test_small(self):
		res = f([1, 2])
		self.assertEqual(res, True)

		res = f([2, 1, 1])
		self.assertEqual(res, True)

		res = f([3, 2, 1])
		self.assertEqual(res, False)

	def test_weird(self):
		res = f([0, 0, 0, 0, 0, 0])
		self.assertEqual(res, True)

		res = f([1, 1, 1, 1, 1, 0])
		self.assertEqual(res, True)

	def test_big(self):
		pass

	def test_random(self):
		inp = [random.randint(1, 10000) for _ in range(6)]
		print(inp)
		self.assertEqual(f(inp), res_f(inp))


if __name__ == '__main__':
	unittest.main()