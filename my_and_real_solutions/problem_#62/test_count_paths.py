import random 
import unittest

from count_paths import count_paths as f 


class TestCountPaths(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f(2, 2)
		self.assertEqual(res, 2)

		res = f(5, 5)
		self.assertEqual(res, 70)

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()