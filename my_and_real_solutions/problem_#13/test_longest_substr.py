import unittest
import random
import string

from sol import longest_substr as _
from longest_substr import longest_substr as l


class TestLongestSubstr(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		s = 'abcba'
		k = 2
		self.assertEqual(l(s, k), _(s, k))

	def test_empty_or_zero_distinct(self):
		s = ['', 'askdjflsdj']
		k = [3, 0]
		for i in range(len(s)):
			self.assertEqual(l(s[i], k[i]), _(s[i], k[i]))

	def test_simple_and_small(self):
		s = ['aaa', 'abc', 'aabbccd']
		k = [1, 3, 3]
		for i in range(len(s)):
			self.assertEqual(l(s[i], k[i]), _(s[i], k[i]))

	def test_big(self):
		s = ''
		for i in range(1000):
			s += random.randint(1, 10) * random.choice(string.ascii_lowercase)
		k = random.randint(1, 10)
		self.assertEqual(l(s, k), _(s, k))


if __name__ == '__main__':
	unittest.main()