"""
This problem was asked by Amazon.

Given an integer k and a string s, 
find the length of the longest substring 
that contains at most k distinct characters.

For example, given s = "abcba" and k = 2, 
the longest substring with k distinct characters is "bcb".
"""


def longest_substr(s, k):
	if k == 0:
		return 0

	temp = ''
	symbols = 0
	res = 0
	l = len(s)

	for i in range(l):
		temp += s[i]
		symbols += 1

		for j in range(i, l):
			if i == j: 
				continue

			if s[j] not in temp:
				symbols += 1

			if symbols > k:
				break

			temp += s[j]

		if len(temp) > res:
			res = len(temp)
		temp = ''
		symbols = 0


	return res
