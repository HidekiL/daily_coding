import random
import unittest

from merge_intervals import merge as f


class TestMerge(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([(1, 3), (5, 8), (4, 10), (20, 25)])
		self.assertEqual(res, [(1, 3), (4, 10), (20, 25)])

	def test_my_example(self):
		res = f([(1, 3), (5, 8), (2, 10), (20, 25)])
		self.assertEqual(res, [(1, 10), (20, 25)])

		res = f([(1, 3), (5, 8), (2, 10), (9, 25)])
		self.assertEqual(res, [(1, 25)])

	def test_empty(self):
		res = f([])
		self.assertEqual(res, [])
		
	def test_small(self):
		res = f([(1, 4), (2, 6)])
		self.assertEqual(res, [(1, 6)])

		res = f([(1, 5), (6, 12)])
		self.assertEqual(res, [(1, 5), (6, 12)])

	def test_weird(self):
		res = f([(0, 1), (1, 2), (2, 3), (3, 4), (4, 5)])
		self.assertEqual(res, [(0, 5)])

		res = f([(0, 1) for _ in range(10)])
		self.assertEqual(res, [(0, 1)])

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()