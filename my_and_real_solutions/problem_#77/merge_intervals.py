"""
This problem was asked by Snapchat.

Given a list of possibly overlapping intervals, return a new list of intervals where all overlapping intervals have
been merged.

The input list is not necessarily ordered in any way.

For example, given [(1, 3), (5, 8), (4, 10), (20, 25)], you should return [(1, 3), (4, 10), (20, 25)].
"""


def is_mergeable(interv1, interv2):
	return interv2[0] <= interv1[0] <= interv2[1] or interv2[0] <= interv1[1] <= interv2[1] \
			or interv1[0] <= interv2[0] <= interv1[1] or interv1[0] <= interv2[1] <= interv1[1]


def merge(intervals):
	merged_intervals = []

	for interval in intervals:
		if not merged_intervals:
			merged_intervals.append(interval)
			continue

		to_remove = []
		for merged_interval in merged_intervals:
			if is_mergeable(interval, merged_interval):
				to_remove.append(merged_interval)
				interval = (min(interval[0], merged_interval[0]), max(interval[1], merged_interval[1]))

		for interv in to_remove:
			merged_intervals.remove(interv)

		merged_intervals.append(interval)

	return merged_intervals

