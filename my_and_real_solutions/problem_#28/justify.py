"""
This problem was asked by Palantir.

Write an algorithm to justify text. 
Given a sequence of words and an integer line length k, 
return a list of strings which represents each line, fully justified.

More specifically, you should have as many words as possible in each line. 
There should be at least one space between each word. 
Pad extra spaces when necessary so that each line has exactly length k. 
Spaces should be distributed as equally as possible, 
with the extra spaces, if any, distributed starting from the left.

If you can only fit one word on a line, then you should pad the right-hand side with spaces.


Each word is guaranteed not to be longer than k.

For example, given the list of words 
["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"] and k = 16, 
you should return the following:

["the  quick brown", # 1 extra space on the left
"fox  jumps  over", # 2 extra spaces distributed evenly
"the   lazy   dog"] # 4 extra spaces distributed evenly
"""


import math


def create_line_words(words, k):
	res = []
	line = []
	left = k
	for word in words:	
		if left - len(word) < 0:
			left = k
			res.append(line)
			line = []
			
		left -= len(word) + 1
		line.append(word)

	res.append(line)

	return res


def line_lengthes(line_words):
	res = []
	for words in line_words:
		res.append(sum(len(word) for word in words))

	return res


def justify(words, k):
	line_words = create_line_words(words, k)
	lengthes = line_lengthes(line_words)

	res = []
	line = ""
	for i in range(len(line_words)):
		length = lengthes[i]

		allowed_spaces = k - length
		try:
			spaces_between = math.ceil((k - length) / (len(line_words[i])-1))
		except:
			res.append(line_words[i][0] + allowed_spaces*" ")
			continue

		for word in line_words[i]:
			if allowed_spaces < spaces_between:
				line += word + allowed_spaces * " "
				allowed_spaces = 0
				continue

			line += word + spaces_between * " "
			allowed_spaces -= spaces_between

		res.append(line)
		line = ""

	return res
