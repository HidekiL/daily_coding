import random 
import unittest

from justify import justify as f 
from sol import justify_text as _


class TestIsBalanced(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f(["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"], 16)
		self.assertEqual(res, ["the  quick brown",
							   "fox  jumps  over",
							   "the   lazy   dog"])
		
	def test_empty(self):
		res = f([""], 10)
		self.assertEqual(res, [" "*10])
		
	def test_small(self):
		res = f(["dt", "man", "hello"], 7)
		self.assertEqual(res, _(["dt", "man", "hello"], 7))

	def test_weird(self):
		inp = ["sdkfjdjfkj", "lkjk", "sdfjiojeifeifjefjierji", "djf", "sdjfk", "df", "d", "de", "sdkjfdljf"]
		res = f(inp, 24)
		self.assertEqual(res, _(inp, 24))

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()