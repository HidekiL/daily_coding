import random 
import unittest

from num_of_inversions import num_of_inversions as f 


class TestNumofinversions(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([2, 4, 1, 3, 5])
		self.assertEqual(res, 3)

		res = f([1, 2, 3, 4, 5])
		self.assertEqual(res, 0)

		res = f([5, 4, 3, 2, 1])
		self.assertEqual(res, 10)

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()