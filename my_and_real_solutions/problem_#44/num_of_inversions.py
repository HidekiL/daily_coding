"""
This problem was asked by Google.

We can determine how "out of order" an array A is by counting the number of inversions it has. 
Two elements A[i] and A[j] form an inversion if A[i] > A[j] but i < j. That is, a smaller element appears after a larger element.

Given an array, count the number of inversions it has. Do this faster than O(N^2) time.

You may assume each element in the array is distinct.

For example, a sorted list has zero inversions. The array [2, 4, 1, 3, 5] has three inversions: 
(2, 1), (4, 1), and (4, 3). The array [5, 4, 3, 2, 1] has ten inversions: every distinct pair forms an inversion.
"""


# WRONG


def num_of_inversions(arr):
	sorted_arr = sorted(arr)
	
	count_dict = {}
	decrease_by = 0

	for ind, num in enumerate(sorted_arr):
		count_dict[num] = ind
	
	count_inversions = 0

	for i in arr:
		count_inversions += count_dict[i] - decrease_by
		decrease_by += 1
		del count_dict[i]

	return count_inversions