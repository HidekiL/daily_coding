import random 
import unittest

from itinerary import itinerary as f 


class TestItinerary(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res1 = f([('SFO', 'HKO'), ('YYZ', 'SFO'), ('YUL', 'YYZ'), ('HKO', 'ORD')], 'YUL')
		self.assertEqual(res1, ['YUL', 'YYZ', 'SFO', 'HKO', 'ORD'])

		res2 = f([('SFO', 'COM'), ('COM', 'YYZ')], 'COM')
		self.assertEqual(res2, None)

		res3 = f([('A', 'B'), ('A', 'C'), ('B', 'C'), ('C', 'A')], 'A')
		self.assertEqual(res3, ['A', 'B', 'C', 'A', 'C'])

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		res = f([('A', 'B')], 'A')
		self.assertEqual(res, ['A', 'B'])

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()