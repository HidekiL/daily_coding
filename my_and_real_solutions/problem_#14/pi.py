"""
This problem was asked by Google.

The area of a circle is defined as πr^2. 
Estimate π to 3 decimal places using a Monte Carlo method.

Hint: The basic equation of a circle is x2 + y2 = r2.
"""

import random


def calculate_pi():
	points_in_circle = 0
	total_points = 0

	for i in range(10000000):
		random_x = random.uniform(0, 1)
		random_y = random.uniform(0, 1)

		if random_x**2 + random_y**2 <= 1:
			points_in_circle += 1

	return 4 * (points_in_circle / 10000000)


print(calculate_pi())