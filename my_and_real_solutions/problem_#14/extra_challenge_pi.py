"""
Estimate pi using parallelism.

for this number of iterations it was 3 times faster 
than without multiprocessing
"""

import concurrent.futures as conc
import random
import multiprocessing as mp


def calculate_pi(iteration):
	points_in_circle = 0

	for i in range(iteration):
		random_x = random.uniform(0, 1)
		random_y = random.uniform(0, 1)

		if random_x**2 + random_y**2 <= 1:
			points_in_circle += 1

	return points_in_circle


iterations = 10000000
core = mp.cpu_count()
it = int(iterations/core)
in_circle = 0

with conc.ProcessPoolExecutor() as executor:
	results = [executor.submit(calculate_pi, it) for i in range(core)]

	for f in conc.as_completed(results):
		in_circle += f.result()

print(4*(in_circle/iterations))


