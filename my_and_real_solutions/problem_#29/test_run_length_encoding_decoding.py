import random 
import unittest

from run_length_encoding_decoding import encode, decode 
from sol import encode_, decode_


class TestIsBalanced(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res_encode = encode("AAAABBBCCDAA")
		res_decode = decode("4A3B2C1D2A")
		self.assertEqual(res_encode, "4A3B2C1D2A")
		self.assertEqual(res_decode, "AAAABBBCCDAA")
		
	def test_empty(self):
		res_encode = encode("")
		res_decode = decode("")
		self.assertEqual("", res_encode)
		self.assertEqual("", res_decode)
		
	def test_small(self):
		res_encode = encode("ABC")
		res_decode = decode("1A1B1C")
		self.assertEqual("1A1B1C", res_encode)
		self.assertEqual("ABC", res_decode)

	def test_weird(self):
		a = "AAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBCCCCCCCCCCCCCCCCCCDDDDDDDDD"
		b = "15A1B23H13A23L1I"
		self.assertEqual(encode(a), encode_(a))
		self.assertEqual(decode(b), decode_(b))

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()