"""
This problem was asked by Amazon.

Run-length encoding is a fast and simple method of encoding strings. 
The basic idea is to represent repeated successive characters as a single count and character. 
For example, the string "AAAABBBCCDAA" would be encoded as "4A3B2C1D2A".

Implement run-length encoding and decoding. 
You can assume the string to be encoded have no digits and 
consists solely of alphabetic characters. 
You can assume the string to be decoded is valid.
"""


def encode(s):
	if not s:
		return '' 

	curr_char = s[0]
	count = 0
	mess = ''

	for char in s:
		if char == curr_char:
			count += 1
		else:
			mess += str(count) + curr_char
			curr_char = char
			count = 1
	mess += str(count) + curr_char

	return mess


def decode(s):
	mess = ''
	count = ''
	for char in s:
		if char.isdigit():
			count += char
		else:
			mess += int(count)*char
			count = ''

	return mess
