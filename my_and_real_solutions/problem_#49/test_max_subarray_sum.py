import random 
import unittest

from max_subarray_sum import max_sum as f 
from sol import max_subarray_sum as _


class TestMaxSum(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([34, -50, 42, 14, -5, 86])
		self.assertEqual(res, 137)

		res = f([-5, -1, -2, -5])
		self.assertEqual(res, 0)

	def test_my_example(self):
		res = f([3, -13, 9, -1, -12, 9, 4, 20, -17, 0])
		self.assertEqual(res, 33)
		
	def test_empty(self):
		res = f([])
		self.assertEqual(res, 0)
		
	def test_small(self):
		res = f([-3, 2])
		self.assertEqual(res, 2)

		res = f([1, -2, 2])
		self.assertEqual(res, 2)

	def test_weird(self):
		res = f([-1, -2, -3, 0, -2, 2, 3, 4, 5, 6, -2, -3, -4, 0, -5, -5])
		self.assertEqual(res, 20)

		res = f([-1, -2, -3, 0, -2, 2, 3, 4, 5, 6, -2, -33, -4, 0, -5, 21, -5])
		self.assertEqual(res, 21)

	def test_big(self):
		pass

	def test_random(self):
		inp = []
		for i in range(100):
			inp.append(random.randint(-100, 100))
		self.assertEqual(f(inp), _(inp))


if __name__ == '__main__':
	unittest.main()