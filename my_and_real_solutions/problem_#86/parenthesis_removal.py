"""
This problem was asked by Google.

Given a string of parentheses, write a function to compute the minimum number of parentheses to be removed to make
the string valid (i.e. each open parenthesis is eventually closed).

For example, given the string "()())()", you should return 1. Given the string ")(", you should return 2,
since we must remove all of them. """


def min_removal(parentheses):
	open_brackets = []
	count = 0

	for parenthesis in parentheses:
		if not open_brackets and parenthesis != '(':
			count += 1
		elif parenthesis == ')' and open_brackets:
			open_brackets.pop()
		else:
			open_brackets.append('(')

	return count + len(open_brackets)

