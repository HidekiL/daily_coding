import random 
import unittest

from parenthesis_removal import min_removal as f 
from sol import count_invalid_parenthesis as res_f


class TestMinRemoval(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f("()())()")
		self.assertEqual(res, 1)

		res = f(")(")
		self.assertEqual(res, 2)

	def test_my_example(self):
		res = f("()())())")
		self.assertEqual(res, 2)
		
	def test_empty(self):
		res = f("")
		self.assertEqual(res, 0)
		
	def test_small(self):
		res = f("))")
		self.assertEqual(res, 2)

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		inp = ""
		for i in range(1000):
			inp += random.choice([')', '('])
		self.assertEqual(f(inp), res_f(inp))


if __name__ == '__main__':
	unittest.main()