"""
This problem was asked by Dropbox.

Sudoku is a puzzle where you're given a partially-filled 9 by 9 grid with digits. 
The objective is to fill the grid with the constraint that every row, column, and box 
(3 by 3 subgrid) must contain all of the digits from 1 to 9.

Implement an efficient sudoku solver.
"""


# WRONG SOLUTION


def get_quarter(x, y):
	return (x//3, y//3)


def is_valid(puzzle, x, y, num):
	for i in range(9):
		if num == puzzle[i][y]:
			return False

	if num in puzzle[x]:
		return False 

	quarter_x, quarter_y = get_quarter(x, y)
	for i in range(3):
		for j in range(3):
			curr_x, curr_y = quarter_x * 3 + i, quarter_y * 3 + j
			if puzzle[curr_x][curr_y] == num:
				return False

	return True


def sudoku_solver(puzzle):
	found = False
	x, y = None, None
	for i in range(9):
		for j in range(9):
			if puzzle[i][j] == 0:
				x, y = i, j
				for k in range(1, 10):
					if is_valid(puzzle, x, y, k):
						puzzle[x][y] = k
						puzzle = sudoku_solver(puzzle)
					else:
						continue
	if x == None or y == None:
		return

	return puzzle
