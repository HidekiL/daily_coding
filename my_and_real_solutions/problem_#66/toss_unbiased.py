"""
This problem was asked by Square.

Assume you have access to a function toss_biased() which returns 0 or 1 with a probability that's not 50-50 (but also
not 0-100 or 100-0). You do not know the bias of the coin.

Write a function to simulate an unbiased coin toss.
"""


def toss_biased():
    import random

    head_prob = 0.51
    r = random.uniform(0, 1)

    if r < head_prob:
        return '1'
    else:
        return '0'


def toss_unbiased():
    toss = {'0': 0, '1': 0}

    first_toss = toss_biased()
    toss[first_toss] += 1

    while first_toss == toss_biased():
        toss[first_toss] += 1

    for _ in range(max(toss.values())):
        if toss_biased() != first_toss:
            return '0' if first_toss == '1' else '1'

    return '1' if first_toss == '1' else '0'


one = 0
zero = 0
for i in range(1000):
    if toss_unbiased() == '1':
        one += 1
    else:
        zero += 1

print('0: ', zero)
print('1: ', one)
