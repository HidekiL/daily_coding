"""
This problem was asked by Apple.

Implement a queue using two stacks. 
Recall that a queue is a FIFO (first-in, first-out) data structure with the following methods: 
enqueue, which inserts an element into the queue, and dequeue, which removes it.
"""


class Queue:
	def __init__(self):
		self.stack1 = []
		self.stack2 = []

	def enqueue(self, el):
		for i in range(len(self.stack1)-1, -1, -1):
			self.stack2.append(self.stack1[i])
			self.stack1.pop()

		self.stack1.append(el)

		for i in range(len(self.stack2)-1, -1, -1):
			self.stack1.append(self.stack2[i])
			self.stack2.pop()

	def dequeue(self):
		return self.stack1.pop()