"""
This problem was asked by Google.

Implement locking in a binary tree. 
A binary tree node can be locked or unlocked 
only if all of its descendants or ancestors are not locked.

Design a binary tree node class with the following methods:

is_locked, which returns whether the node is locked
lock, which attempts to lock the node. 
If it cannot be locked, then it should return false. 
Otherwise, it should lock it and return true.

unlock, which unlocks the node. If it cannot be unlocked, 
then it should return false. Otherwise, it should unlock it and return true.

You may augment the node to add parent pointers or any other property you would like. 
You may assume the class is used in a single-threaded program, 
so there is no need for actual locks or mutexes. 
Each method should run in O(h), where h is the height of the tree.
"""


class Tree:
	def __init__(self, value, parent=None):
		self.value = value
		self.left = None
		self.right = None
		self.parent = parent
		self.locked = False

	def is_locked(self):
		return self.locked 

	def unlocked_ancestors(self):
		if self.parent is None:
			return False

		while self.parent != None:
			if self.parent.is_locked():
				return False

			self = self.parent

		return True

	def unlocked_descendants(self):
		queue = []

		if self.left is not None: 
			queue.append(self.left) 

		if self.right is not None: 
			queue.append(self.right)

		if not queue:
			return False

		while queue:
			s = queue.pop(0)

			if s.is_locked():
				return False

			if s.left is not None: 
				queue.append(s.left) 

			if s.right is not None: 
				queue.append(s.right)

		return True


	def lock(self):
		if self.unlocked_ancestors() or self.unlocked_descendants():
			self.locked = True
			return True
		else:
			return False

	def unlock(self):
		if self.unlocked_ancestors() or self.unlocked_descendants():
			self.locked = False
			return True
		else:
			return False

	



tree = Tree(1)
tree.left = Tree(2, tree)
tree.right = Tree(3, tree)
tree.left.left = Tree(4, tree.left)
tree.left.right = Tree(8, tree.left)
tree.left.left.left = Tree(5, tree.left.left)
tree.left.left.right = Tree(6, tree.left.left)
tree.left.left.right.right = Tree(7, tree.left.left.right)
tree.right.left = Tree(9, tree.right)
tree.right.left.right = Tree(10, tree.right.left)
tree.right.left.right.left = Tree(11, tree.right.left.right)
tree.right.left.right.right = Tree(12, tree.right.left.right)

tree.left.left.lock()    # 4
print(tree.left.left.locked)
tree.left.left.left.lock() # 5
print(tree.left.left.left.locked)
tree.left.left.right.right.lock() # 7
print(tree.left.left.right.right.locked)
tree.left.left.right.lock() # 6
print(tree.left.left.right.locked)
tree.lock() # 1
print(tree.lock())
tree.left.right.lock() # 8
print(tree.left.right.locked)
tree.left.lock()
print(tree.left.locked) # 2

print(tree.left.unlock()) # 8
print(tree.left.locked)