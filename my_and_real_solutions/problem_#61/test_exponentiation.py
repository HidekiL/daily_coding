import random 
import unittest

from exponentiation import pow as f 


class TestPow(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f(2, 10)
		self.assertEqual(res, 1024)

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()