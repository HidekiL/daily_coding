"""
This problem was asked by Amazon.

Given a string, find the longest palindromic contiguous substring. If there are more than one with the maximum length, return any one.

For example, the longest palindromic substring of "aabcdcb" is "bcdcb". The longest palindromic substring of "bananas" is "anana".
"""


def is_palindrome(s):
	return s == s[::-1]


def longest_palin_substr(s):
	if is_palindrome(s):
		return s

	if len(s) <= 1:
		return s

	a = longest_palin_substr(s[1:])
	b = longest_palin_substr(s[:-1])

	if len(a) > len(b):
		return a

	else:
		return b

