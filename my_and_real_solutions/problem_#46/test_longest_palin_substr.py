import random 
import unittest

from longest_palin_substr import longest_palin_substr as f 


class TestLongestPalinSubstr(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f("aabcdcb")
		self.assertEqual(res, "bcdcb")

		res = f("bananas")
		self.assertEqual(res, "anana")

	def test_my_example(self):
		pass
		
	def test_empty(self):
		res = f("")
		self.assertEqual(res, "")
		
	def test_small(self):
		res = f("h")
		self.assertEqual("h", res)

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()