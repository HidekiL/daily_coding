"""
This question was asked by Google.

Given an integer n and a list of integers l, write a function that randomly generates a number from 0 to n-1 that
isn't in l (uniform). """

from _collections import defaultdict


def generate_random(n, l):
    import random

    return random.choice([item for item in range(n) if item not in l])


d = defaultdict(int)

for i in range(10000):
    d[generate_random(10, [1, 2, 5, 7, 15])] += 1

for key in sorted(d.keys()):
    print(str(key), ': ', d[key])
