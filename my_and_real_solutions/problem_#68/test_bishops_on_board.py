import random 
import unittest

from bishops_on_board import attacking_pairs as f 


class TestAttackingPairs(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([(0, 0), (1, 2), (2, 2), (4, 0)], 5)
		self.assertEqual(res, 2)

	def test_my_example(self):
		pass
		
	def test_empty(self):
		res = f([], 2)
		self.assertEqual(res, 0)
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()