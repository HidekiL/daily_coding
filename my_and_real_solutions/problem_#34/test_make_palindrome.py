import random 
import unittest
import string

from make_palindrome import make_palindrome as f 
from sol import make_palindrome as _


class TestIsBalanced(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res1 = f('race')
		self.assertEqual(res1, 'ecarace')
		res2 = f('google')
		self.assertEqual(res2, 'elgoogle')

	def test_my_example(self):
		res = f('okk')
		self.assertEqual(res, 'okko')
		
	def test_empty(self):
		res = f('')
		self.assertEqual(res, '')
		
	def test_small(self):
		res = f('t')
		self.assertEqual(res, 't')

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		s = ''
		for i in range(10):
			s += random.choice(string.ascii_lowercase)
		
		self.assertEqual(f(s), _(s))


if __name__ == '__main__':
	unittest.main()