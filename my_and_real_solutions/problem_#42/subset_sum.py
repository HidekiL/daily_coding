"""
This problem was asked by Google.

Given a list of integers S and a target number k, 
write a function that returns a subset of S that adds up to k. 
If such a subset cannot be made, then return null.

Integers can appear more than once in the list. 
You may assume all numbers in the list are positive.

For example, given S = [12, 1, 61, 5, 9, 2] and k = 24, return [12, 9, 2, 1] since it sums up to 24.
"""


def helper(set_):
	res = []
	sub = []
	res.append(set_)

	for i in range(len(set_)):
		res.append(set_[:i]+set_[i+1:])
		sub += (power_set(set_[:i]+set_[i+1:]))

	return res + sub


def power_set(set_):
	res = helper(set_)
	seen = []
	real_result = []

	for subset in res:
		if subset in seen:
			continue

		else:
			seen.append(subset)
			real_result.append(subset)

	return real_result


def subset_sum(s, k):
	subsets = power_set(s)

	for subset in subsets:
		if sum(subset) == k:
			return subset
