import random 
import unittest

from subset_sum import subset_sum as f 


class TestSubset_sum(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([12, 1, 61, 5, 9, 2], 24)
		self.assertEqual(sorted(res), sorted([12, 9, 2, 1]))

	def test_my_example(self):
		pass
		
	def test_empty(self):
		res = f([], 23)
		self.assertEqual(res, None)
		
	def test_small(self):
		res = f([1, 2, 3], 4)
		self.assertEqual([1, 3], res)

		res = f([1, 2, 3], 7)
		self.assertEqual(res, None)

		res = f([1], 1)
		self.assertEqual(res, [1])

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()