import random
import unittest

from paths_value import paths_value as f


class TestPathsValue(unittest.TestCase):
    def setUp(self):
        pass

    def test_example(self):
        res = f('ABACA', [(0, 1),
                          (0, 2),
                          (2, 3),
                          (3, 4)])
        self.assertEqual(res, 3)

        res = f('A', [0, 0])
        self.assertEqual(res, None)

    def test_my_example(self):
        pass

    def test_empty(self):
        pass

    def test_small(self):
        pass

    def test_weird(self):
        pass

    def test_big(self):
        pass

    def test_random(self):
        pass


if __name__ == '__main__':
    unittest.main()
