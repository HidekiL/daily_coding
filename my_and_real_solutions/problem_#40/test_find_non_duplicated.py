import random 
import unittest

from find_non_duplicated import find_non_duplicated as f 


class TestFind_non_duplicated(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res1 = f([6, 1, 3, 3, 3, 6, 6])
		self.assertEqual(res1, 1)

		res2 = f([13, 19, 13, 13])
		self.assertEqual(res2, 19)

	def test_my_example(self):
		res = f([1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6])
		self.assertEqual(res, 7)
		
	def test_empty(self):
		res = f([])
		self.assertEqual(res, [])
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()