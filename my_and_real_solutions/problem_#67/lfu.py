"""
This problem was asked by Google.

Implement an LFU (Least Frequently Used) cache. It should be able to be initialized with a cache size n, and contain
the following methods:

set(key, value): sets key to value. If there are already n items in the cache and we are adding a new item,
then it should also remove the least frequently used item. If there is a tie, then the least recently used key should
be removed. get(key): gets the value at key. If no such key exists, return null. Each operation should run in O(1)
time. """


class Node:
    def __init__(self, key, val):
        self.key = key
        self.val = val
        self.prev = None
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = Node(None, 'head')
        self.tail = Node(None, 'tail')
        self.head.next = self.tail
        self.tail.prev = self.head

    def change(self, key):
        pass


class LFU:
    def __init__(self, n):
        self.n = n
        self.dict = {}
        self.list = LinkedList()

    def set(self, key, val):
        if key in self.dict:
            return {key: val}
        else:
            self.dict[key] = {'value': val, 'freq': 0}

    def get(self, key):
        if key in self.dict:
            self.dict[key]['freq'] += 1
            self.list.change(key)
            return self.dict[key]['value']
        else:
            return None
