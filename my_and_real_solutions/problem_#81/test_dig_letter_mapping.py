import random 
import unittest

from dig_letter_mapping import dig_letter_map as f 


class TestDigLetterMap(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f({'2': ['a', 'b', 'c'], '3': ['d', 'e', 'f']}, '23')
		self.assertEqual(res, ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"])

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()