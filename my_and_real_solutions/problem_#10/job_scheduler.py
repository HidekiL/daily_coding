"""
This problem was asked by Apple.

Implement a job scheduler 
which takes in a function f and an integer n, 
and calls f after n milliseconds
"""


import datetime


def job_schedule(f, n):
	while True:
		dt_job = datetime.datetime.now().timestamp() * 1000 + n
		while True:
			dt_millis = datetime.datetime.now().timestamp() * 1000
			if dt_millis >= dt_job:
				return f()
