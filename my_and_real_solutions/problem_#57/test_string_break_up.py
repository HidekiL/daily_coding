import random 
import unittest

from string_break_up import string_break_up as f 


class TestStringBreakUp(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f("the quick brown fox jumps over the lazy dog", 10)
		self.assertEqual(["the quick", "brown fox", "jumps over", "the lazy", "dog"], res)

	def test_my_example(self):
		res = f('hello world! this is unreal!', 7)
		self.assertEqual(['hello', 'world!', 'this is', 'unreal!'], res)
		
	def test_empty(self):
		res = f('', 10)
		self.assertEqual(res, [])
		
	def test_small(self):
		res = f('small text', 4)
		self.assertEqual(res, None)

		res = f('small text', 10)
		self.assertEqual(['small text'], res)

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()