"""
This problem was asked by Amazon.

Given a string s and an integer k, break up the string into multiple lines such that each line has a length of k or less. 
You must break it up so that words don't break across lines. 
Each line has to have the maximum possible amount of words. If there's no way to break the text up, then return null.

You can assume that there are no spaces at the ends of the string and that there is exactly one space between each word.

For example, given the string "the quick brown fox jumps over the lazy dog" and k = 10, you should return: 
["the quick", "brown fox", "jumps over", "the lazy", "dog"]. No string in the list has a length of more than 10.
"""


def string_break_up(s, k):
	res = []

	curr_count = 0
	curr_line = ""
	words = s.split()
	for word in words:
		if len(word) > k:
			return None

		curr_count += len(word)
		if curr_count == k:
			curr_count = 0
			curr_line += word
			res.append(curr_line.rstrip())
			curr_line = ""
		elif curr_count > k:
			res.append(curr_line.rstrip())
			curr_count = len(word) + 1
			curr_line = word + " "
		else:
			curr_line += word + " "
			curr_count += 1

	if curr_line != "":
		res.append(curr_line.rstrip())
	return res
