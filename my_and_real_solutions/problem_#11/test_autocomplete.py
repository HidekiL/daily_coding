import unittest

from autocomplete import autocomplete


class TestAutocomplete(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = autocomplete('de', ['dog', 'deer', 'deal'])
		self.assertEqual(res, ['deer', 'deal'])

	def test_empty_input(self):
		inp = ['asdkfjdljf', 'sdfjsldkafj','dog', 'deer', 'deal', 'ksajflskd', 'asdjflk', 'asldfjklsd']
		res = autocomplete('', inp)
		self.assertEqual(res, inp)

	def test_empty_strings_list(self):
		res = autocomplete('l', [])
		self.assertEqual(res, [])

	def test_other(self):
		res = autocomplete('home', ['welcome home', 'homeland', 'homeseeker', 'hooome', 'homeless'])
		self.assertEqual(res, ['homeland', 'homeseeker', 'homeless'])


if __name__ == '__main__':
	unittest.main()