import unittest
import random

from longest_absolute_path import longest_absolute_path as l
from sol.py import longest_absolute_path as _


class TestLongestAbsolutePath(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		inp = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"
		self.assertEqual(l(inp), 32)

	def test_no_file(self):
		inp = "dir\n\tsubdir1\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2"
		self.assertEqual(l(inp), 0)

	def test_example2(self):
		inp = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext"
		self.assertEqual(l(inp), 20)

	def test_one_file(self):
		inp = 'text.txt'
		self.assertEqual(l(inp), len(inp))
		

if __name__ == '__main__':
	unittest.main()