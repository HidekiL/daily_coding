"""
This problem was asked by Google.

Suppose we represent our file system by a string in the following manner:

The string "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext" represents:

dir
    subdir1
    subdir2
        file.ext

The directory dir contains an empty sub-directory subdir1 and 
a sub-directory subdir2 containing a file file.ext.

The string "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext" represents:

dir
    subdir1
        file1.ext
        subsubdir1
    subdir2
        subsubdir2
            file2.ext

The directory dir contains two sub-directories subdir1 and subdir2. 
subdir1 contains a file file1.ext and an empty second-level sub-directory subsubdir1.
subdir2 contains a second-level sub-directory subsubdir2 containing a file file2.ext.

We are interested in finding the longest (number of characters) 
absolute path to a file within our file system. 
For example, in the second example above, the longest absolute path is 
"dir/subdir2/subsubdir2/file2.ext", and its length is 32 (not including the double quotes).

Given a string representing the file system in the above format, 
return the length of the longest absolute path to a file in the abstracted file system. 
If there is no file in the system, return 0.

Note:

The name of a file contains at least a period and an extension.

The name of a directory or sub-directory will not contain a period.
"""


# WRONG SOLUTION
def is_file(s):
	return '.' in s


def longest_absolute_path(file_system):
	if '.' not in file_system:
		return 0

	if '\t' not in file_system:
		return len(file_system)

	dir_levels = []

	file_system_sp = file_system.split('\n')

	for i in range(len(file_system_sp)):
		if is_file(file_system_sp[i]):
			dir_levels.append(-1)
		else:
			dir_levels.append(file_system_sp[i].count('\t'))

	curr_path_length = 0
	max_path_length = 0
	files_and_folders = []

	for i in file_system_sp:
		files_and_folders.append(i.strip())

	for ind, dir_level in enumerate(dir_levels):
		if dir_level != -1:
			continue

		curr_path_length += len(files_and_folders[ind]) + 1
		curr_dir_level = dir_levels[ind-1] 
		curr_path_length += len(files_and_folders[ind-1]) + 1
		for i in range(ind-2, -1, -1):
			if dir_levels[i] == curr_dir_level - 1:
				curr_path_length += len(files_and_folders[i]) + 1
				curr_dir_level -= 1

			if curr_dir_level == -1:
				break

		if curr_path_length > max_path_length:
			max_path_length = curr_path_length

		curr_path_length = 0

	return max_path_length - 1