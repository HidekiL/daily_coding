"""
This problem was asked by Microsoft.

A number is considered perfect if its digits sum up to exactly 10.

Given a positive integer n, return the n-th perfect number.

For example, given 1, you should return 19. Given 2, you should return 28.
"""


def is_perfect(num):
	s = 0
	while num:
		s += num % 10
		num //= 10
	return s == 10


def perfect_number(n):
	num = 19

	if n == 1:
		return num

	for _ in range(1, n):
		num += 9

		while not is_perfect(num):
			num += 9

	return num
