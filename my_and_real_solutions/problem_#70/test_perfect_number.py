import random 
import unittest

from perfect_number import perfect_number as f
from sol import perfect as _

class TestPerfectNumber(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f(1)
		self.assertEqual(res, 19)

		res = f(2)
		self.assertEqual(res, 28)

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		n = random.randint(1, 1000)
		self.assertEqual(f(n), _(n))

if __name__ == '__main__':
	unittest.main()