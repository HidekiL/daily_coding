import random
import unittest

from ordered_rows import cols_to_remove as f
from sol import bad_cols as _


class TestColsToRemove(unittest.TestCase):
    def setUp(self):
        pass

    def test_example(self):
        res = f([['c', 'b', 'a'],
                 ['d', 'a', 'f'],
                 ['g', 'h', 'i']])
        self.assertEqual(res, 1)

        res = f([['a', 'b', 'c', 'd', 'e', 'f']])
        self.assertEqual(res, 0)

        res = f([['z', 'w', 'x'],
                 ['w', 'v', 'u'],
                 ['t', 's', 'r']])
        self.assertEqual(res, 3)

    def test_my_example(self):
        res = f([['c', 'b', 'a'],
                 ['d', 'd', 'f'],
                 ['g', 'h', 'i']])
        self.assertEqual(res, 0)

    def test_empty(self):
        pass

    def test_small(self):
        pass

    def test_weird(self):
        pass

    def test_big(self):
        pass

    def test_random(self):
        import string

        inp = [[random.choice(string.ascii_lowercase) for i in range(10)] for j in range(4)]
        self.assertEqual(f(inp), _(inp))


if __name__ == '__main__':
    unittest.main()
