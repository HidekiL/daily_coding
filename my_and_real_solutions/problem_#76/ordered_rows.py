"""
This problem was asked by Google.

You are given an N by M 2D matrix of lowercase letters. Determine the minimum number of columns that can be removed
to ensure that each row is ordered from top to bottom lexicographically. That is, the letter at each column is
lexicographically later as you go down each row. It does not matter whether each row itself is ordered
lexicographically.

For example, given the following table:

cba
daf
ghi
This is not ordered because of the a in the center. We can remove the second column to make it ordered:

ca
df
gi
So your function should return 1, since we only needed to remove 1 column.

As another example, given the following table:

abcdef
Your function should return 0, since the rows are already ordered (there's only one row).

As another example, given the following table:

zyx
wvu
tsr
Your function should return 3, since we would need to remove all the columns to order it.
"""


def cols_to_remove(matrix):
    current_letters = {i: matrix[0][i] for i in range(len(matrix[0]))}
    min_columns_to_remove = 0

    for i in range(1, len(matrix)):
        cols_to_del = []

        for j in current_letters.keys():
            if current_letters[j] < matrix[i][j]:
                current_letters[j] = matrix[i][j]
            else:
                cols_to_del.append(j)
                min_columns_to_remove += 1

        for col in cols_to_del:
            del current_letters[col]

    return min_columns_to_remove


print(cols_to_remove([['p', 'b', 'b', 'x', 'q', 'h', 'e', 'o', 'g', 'g'],
                      ['y', 't', 'n', 's', 'g', 'm', 'f', 'p', 'x', 'p'],
                      ['b', 'r', 'x', 'y', 'e', 'n', 'v', 'o', 'j', 'v'],
                      ['t', 'b', 'z', 'g', 'q', 'v', 'k', 'i', 'b', 'u']]))
