"""
This problem was asked by Google.

A knight's tour is a sequence of moves by a knight on a chessboard such that all squares are visited once.

Given N, write a function to return the number of knight's tours on an N by N chessboard.
"""


# COULDN'T FIND SOLUTION


def is_movable(row, col, seen, n):
	moves = [(row-2, col-1), (row-2, col+1), (row-1, col+2), (row+1, col+2), (row+2, col+1), (row+2, col-1), (row+1, col-2), (row-1, col-2)]

	movable = []

	for move in moves:
		x, y = move

		if x < n and x > 0 and y < n and y > 0 and move not in seen:
			movable.append(seen)

	if len(movable) == 0:
		return False
	else:
		return movable


def helper(n, seen):
	for i in range(n):
		for j in range(n):
			if is_movable(i, j, seen, n)


def knight_tour(n):
	seen = []
	
	helper(n, seen)