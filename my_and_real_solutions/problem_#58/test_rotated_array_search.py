import random 
import unittest

from rotated_array_search import rotated_array_search as f 


class TestRotatedArraySearch(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([13, 18, 25, 2, 8, 10], 8)
		self.assertEqual(res, 4)

	def test_my_example(self):
		res = f([13, 18, 25, 2, 8, 10], 10)
		self.assertEqual(res, 5)

		res = f([13, 18, 25, 2, 8, 10], 13)
		self.assertEqual(res, 0)

		res = f(sorted([13, 18, 25, 2, 8, 10]), 25)
		self.assertEqual(res, 5)

		res = f(sorted([13, 18, 25, 2, 8, 10]), 2)
		self.assertEqual(res, 0)

		res = f(sorted([13, 18, 25, 2, 8, 10]), 13)
		self.assertEqual(res, 3)
		
	def test_empty(self):
		res = f([], 1)
		self.assertEqual(res, None)
		
	def test_small(self):
		res = f([13, 2], 2)
		self.assertEqual(res, 1)

		res = f([2, 13], 2)
		self.assertEqual(res, 0)

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()