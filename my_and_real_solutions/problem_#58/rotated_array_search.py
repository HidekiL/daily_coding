"""
This problem was asked by Amazon.

An sorted array of integers was rotated an unknown number of times.

Given such an array, find the index of the element in the array in faster than linear time. If the element doesn't exist in the array, return null.

For example, given the array [13, 18, 25, 2, 8, 10] and the element 8, return 4 (the index of 8 in the array).

You can assume all the integers in the array are unique.
"""


def binary_search(arr, el):
	mid = len(arr) // 2

	if mid == 0:
		return 0

	if arr[mid-1] == el:
		return mid - 1
	elif arr[mid-1] < el:
		return mid + binary_search(arr[mid:], el)
	else:
		return binary_search(arr[:mid], el)


def find_min_element_index(arr):
	mid = len(arr) // 2

	if arr[mid-1] > arr[mid]:
		return mid
	elif arr[mid] < arr[-1]:
		return find_min_element_index(arr[:mid])
	else:
		return mid + find_min_element_index(arr[mid:])


def rotated_array_search(arr, el):
	if len(arr) == 0:
		return None

	# find where minimum element of array is located
	if arr[0] < arr[-1]:
		min_el_ind = 0
	else:
		min_el_ind = find_min_element_index(arr)
	
	# find number index in array using binary search in sorted subarray
	if arr[min_el_ind] == el:
		return min_el_ind
	elif el <= arr[-1]:
		return min_el_ind + binary_search(arr[min_el_ind:], el)
	else:
		return binary_search(arr[:min_el_ind], el) 

