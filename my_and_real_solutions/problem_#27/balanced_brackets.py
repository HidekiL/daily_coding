"""
This problem was asked by Facebook.

Given a string of round, curly, and 
square open and closing brackets, 
return whether the brackets are balanced (well-formed).

For example, given the string "([])[]({})", you should return true.

Given the string "([)]" or "((()", you should return false.
"""


brackets = {'(': ')', '{': '}', '[': ']'}


def is_balanced(s):
	open_br = []

	for i in s:
		if not open_br and i not in brackets.keys():
			return False

		if i in brackets.keys():
			open_br.append(i)
		elif brackets[open_br[-1]] == i:
			open_br.pop()
		else:
			return False

	return len(open_br) == 0
	