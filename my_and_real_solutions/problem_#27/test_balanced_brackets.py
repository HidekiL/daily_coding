import random 
import unittest

from balanced_brackets import is_balanced as f 
from sol import balance as _


class TestIsBalanced(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res1 = f("([])[]({})")
		self.assertEqual(res1, True)

		res2 = f("([)]")
		self.assertEqual(res2, False)

		res3 = f("((()")
		self.assertEqual(res3, False)

	def test_empty(self):
		res = f("")
		self.assertEqual(res, True)

	def test_small(self):
		res1 = f('[[')
		self.assertEqual(res1, False)

		res2 = f('{}[]')
		self.assertEqual(res2, True)

	def test_weird(self):
		res = f('[{{{{{}}}}}][][[[]]](([[]]))')
		self.assertEqual(res, True)

	def test_big(self):
		res = f('[{{{[]{{[]}()}}()}}][][[[]]](([[]]))[[])()()(){}')
		self.assertEqual(res, False)

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()