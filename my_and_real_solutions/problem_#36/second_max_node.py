"""
This problem was asked by Dropbox.

Given the root to a binary search tree, find the second largest node in the tree.
"""


def second_max(root):
	if root == None:
		return None

	queue = []

	queue.append(root)

	first = root.value
	second = float("-inf")

	while len(queue) > 0:
		node = queue.pop(0)

		if node.left is not None:
			queue.append(node.left)
			if node.left.value > first:
				second = first
				first = node.left.value

			elif node.left.value > second:
				second = node.left.value

		if node.right is not None:
			queue.append(node.right)
			if node.right.value > first:
				second = first
				first = node.right.value

			elif node.right.value > second:
				second = node.right.value

	return second

