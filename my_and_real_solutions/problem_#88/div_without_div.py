"""
This question was asked by ContextLogic.

Implement division of two positive integers without using the division, multiplication, or modulus operators. Return
the quotient as an integer, ignoring the remainder. """


def div_without_div(a, b):
	result = 0

	while a >= b:
		a -= b
		result += 1

	return result
