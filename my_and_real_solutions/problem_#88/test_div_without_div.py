import random 
import unittest

from div_without_div import div_without_div as f 
from sol import divide as res_f


class TestDivWithoutDiv(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		pass

	def test_my_example(self):
		res = f(10, 5)
		self.assertEqual(res, 2)

		res = f(1, 4)
		self.assertEqual(res, 0)

		res = f(15, 2)
		self.assertEqual(res, 7)

		res = f(28, 7)
		self.assertEqual(res, 4)
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		a, b = random.randint(1, 10000), random.randint(1, 1000)
		self.assertEqual(f(a, b), res_f(a, b))


if __name__ == '__main__':
	unittest.main()