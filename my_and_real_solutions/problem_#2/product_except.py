"""
Asked by Uber

Given an array of integers, 
return a new array such that each element at index i of the new array is the product 
of all the numbers in the original array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], 
the expected output would be [120, 60, 40, 30, 24]. 
If our input was [3, 2, 1], the expected output would be [2, 3, 6].

Follow-up: what if you can't use division?
"""


def product(ls):
	"""
	With division
	"""
	if len(ls) < 2:
		return []

	p = 1
	for i in ls:
		p *= i

	res = ls
	for ind, num in enumerate(ls):
		if num == 0:
			continue
		res[ind] = p // num

	return res


def product_no_div(ls):
	"""
	Without division
	"""
	if len(ls) < 2:
		return []

	res = []

	"""
	Calculating products from left and right.
	for [1, 2, 3, 4, 5] example:
	left must be  [1, 1*2, 1*2*3, 1*2*3*4, 1*2*3*4*5] = [1,   2,   6,  24, 120]
	right must be [5*4*3*2*1, 5*4*3*2, 5*4*3, 5*4, 5] = [120, 120, 60, 20, 5]
	"""
	left = []
	right = []
	left_prod = 1
	right_prod = 1

	# last index of list
	right_index = len(ls) - 1

	# fill in right and left lists
	for i in range(len(ls)):
		left_prod *= ls[i]
		right_prod *= ls[right_index]

		left.append(left_prod)
		right.insert(0, right_prod)

		right_index -= 1

	"""
	Manipulating two lists(left and right) to get correct result
	"""
	res.append(right[1])

	i = 1
	while i < len(ls) - 1:
		res.append(left[i-1] * right[i+1])
		i += 1
	res.append(left[i-1])

	return res