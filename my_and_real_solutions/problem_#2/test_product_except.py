import unittest
from product_except import product, product_no_div


class TestProduct(unittest.TestCase):
	def setUp(self):
		pass

	def test_empty(self):
		res = product([])
		self.assertEqual(res, [])

		res = product([])
		self.assertEqual(res, [])

	def test_one_number(self):
		res = product([3])
		self.assertEqual(res, [])

	def test_example(self):
		res1 = product([1, 2, 3, 4, 5])
		res2 = product([3, 2, 1])

		self.assertEqual(res1, [120, 60, 40, 30, 24])
		self.assertEqual(res2, [2, 3, 6])

	def test_weird(self):
		res = product([0, 0, 0, -2, -123, 0, 0, 0, 1, 1, 10**10, -1321321])
		self.assertEqual(res, [0]*12)

		
class TestProductNoDiv(unittest.TestCase):
	def setUp(self):
		pass

	def test_empty(self):
		res = product_no_div([])
		self.assertEqual(res, [])

		res = product_no_div([])
		self.assertEqual(res, [])

	def test_one_number(self):
		res = product_no_div([3])
		self.assertEqual(res, [])

	def test_example(self):
		res1 = product_no_div([1, 2, 3, 4, 5])
		res2 = product_no_div([3, 2, 1])

		self.assertEqual(res1, [120, 60, 40, 30, 24])
		self.assertEqual(res2, [2, 3, 6])

	def test_weird(self):
		res = product_no_div([0, 0, 0, -2, -123, 0, 0, 0, 1, 1, 10**10, -1321321])
		self.assertEqual(res, [0]*12)



	
if __name__ == '__main__':
	unittest.main()