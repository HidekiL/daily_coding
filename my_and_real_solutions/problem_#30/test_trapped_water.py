import random 
import unittest

from trapped_water import trapped_water as f 
from sol import capacity as _


class TestIsBalanced(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res1 = _([2, 1, 2])
		res2 = _([3, 0, 1, 3, 0, 5])
		self.assertEqual(res1, 1)
		self.assertEqual(res2, 8)
		
	def test_my_example(self):
		res = _([4, 1, 5, 3, 6, 2, 5, 0, 5])
		self.assertEqual(res, 13)

	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()