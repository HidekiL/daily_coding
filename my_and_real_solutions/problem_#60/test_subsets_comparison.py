import random 
import unittest

from subsets_comparison import subsets_comparison as f 


class TestSubsetsComparison(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([15, 5, 20, 10, 35, 15, 10])
		self.assertEqual(res, True)

		res = f([15, 5, 20, 10, 35])
		self.assertEqual(res, False)

	def test_my_example(self):
		res = f([15, 5, 20, 10, 60])
		self.assertEqual(res, False)

		res = f([15, 5, 20, 10, 40])
		self.assertEqual(res, True)

		res = f([1, 2, 3, 4, 5, 6, 7])
		self.assertEqual(res, True)
		
	def test_empty(self):
		res = f([])
		self.assertEqual(res, None)
		
	def test_small(self):
		res = f([1, 2])
		self.assertEqual(res, False)

		res = f([1, 2, 3])
		self.assertEqual(res, True)

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()