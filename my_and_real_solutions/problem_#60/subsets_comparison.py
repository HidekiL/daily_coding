"""
This problem was asked by Facebook.

Given a multiset of integers, return whether it can be partitioned into two subsets whose sums are the same.

For example, given the multiset {15, 5, 20, 10, 35, 15, 10}, it would return true, 
since we can split it up into {15, 5, 10, 15, 10} and {20, 35}, which both add up to 55.

Given the multiset {15, 5, 20, 10, 35}, it would return false, since we can't split it up into two subsets that add up to the same sum.
"""


def helper(half_sum, multist):

	for ind, num in enumerate(multist):
		if num == half_sum:
			return True

		elif num > half_sum:
			return False

		else:
		    if helper(half_sum-num, multist[ind+1:]):
		    	return True


def subsets_comparison(multiset):
	s = sum(multiset)

	if s % 2 != 0:
		return False

	else:
		return helper(s//2, sorted(multiset))
