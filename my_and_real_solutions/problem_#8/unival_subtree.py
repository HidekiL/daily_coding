"""
Asked by Google.

A unival tree (which stands for "universal value") 
is a tree where all nodes under it have the same value.

Given the root to a binary tree, 
count the number of unival subtrees.

For example, 
the following tree has 5 unival subtrees:

	   0
	  / \
	 1   0
	    / \
	   1   0
	  / \
	 1   1
 """


def univ_subt(root):
	if root.left is None and root.right is None:
		return True

	if root.left is None:
		if root.right.value == root.value:
			return univ_subt(root.right)
		else:
			return False


	if root.right is None:
		if root.left.value == root.value:
			return univ_subt(root.left)
		else:
			return False


	if root.value != root.left.value or root.value != root.right.value:
		return False

	return univ_subt(root.left) and univ_subt(root.right)


def count_subt(root):
	if root is None:
		return False

	count = 0
	count += univ_subt(root) + count_subt(root.left) + count_subt(root.right)
	return count