"""
This problem was asked by Google.

The power set of a set is the set of all its subsets. 
Write a function that, given a set, generates its power set.

For example, given the set {1, 2, 3}, it should 
return {{}, {1}, {2}, {3}, {1, 2}, {1, 3}, {2, 3}, {1, 2, 3}}.

You may also use a list or array to represent a set.
"""


def helper(set_):
	res = []
	sub = []
	res.append(set_)

	for i in range(len(set_)):
		res.append(set_[:i]+set_[i+1:])
		sub += (power_set(set_[:i]+set_[i+1:]))

	return res + sub


def power_set(set_):
	res = helper(set_)
	seen = []
	real_result = []

	for subset in res:
		if subset in seen:
			continue

		else:
			seen.append(subset)
			real_result.append(subset)

	return real_result
