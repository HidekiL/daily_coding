"""
This problem was asked by Google.

Given pre-order and in-order traversals of a binary tree, write a function to reconstruct the tree.

For example, given the following preorder traversal:

[a, b, d, e, c, f, g]

And the following inorder traversal:

[d, b, e, a, f, c, g]

You should return the following tree:

    a
   / \
  b   c
 / \ / \
d  e f  g
"""


class Tree:
	def __init__(self, val):
		self.val = val
		self.left = None
		self.right = None


def pre_order_tree_reconstruction(pre_order):
	if len(pre_order) == 1:
		return Tree(pre_order[0])
	
	root = Tree(pre_order[0])

	mid = len(pre_order) // 2
	left = pre_order[1:mid+1]
	right = pre_order[mid+1:]

	root.left = pre_order_tree_reconstruction(left)
	root.right = pre_order_tree_reconstruction(right)
	return root


def in_order_tree_reconstruction(in_order):
	if len(in_order) == 1:
		return Tree(in_order[0])

	mid = len(in_order) // 2

	root = Tree(in_order[mid])

	left = in_order[:mid]
	right = in_order[mid+1:]

	root.left = in_order_tree_reconstruction(left)
	root.right = in_order_tree_reconstruction(right)
	return root


x = pre_order_tree_reconstruction(['a', 'b', 'd', 'e', 'c', 'f', 'g'])
print(x.val, x.left.val, x.left.left.val, x.left.right.val, x.right.val, x.right.left.val, x.right.right.val)

x = in_order_tree_reconstruction(['d', 'b', 'e', 'a', 'f', 'c', 'g'])
print(x.val, x.left.val, x.left.left.val, x.left.right.val, x.right.val, x.right.left.val, x.right.right.val)