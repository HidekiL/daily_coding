import random 
import unittest

from search_word import search_word as f 


class TestSearchWord(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([['F', 'A', 'C', 'I'],
				  ['O', 'B', 'Q', 'P'],
				  ['A', 'N', 'O', 'B'],
				  ['M', 'A', 'S', 'S']], 'FOAM')
		self.assertEqual(res, True)

	def test_my_example(self):
		pass
		
	def test_empty(self):
		res = f([[]], 'A')
		self.assertEqual(res, False)
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()