"""
This problem was asked by Microsoft.

Given a 2D matrix of characters and a target word, 
write a function that returns whether the word can be found in the matrix by going left-to-right, or up-to-down.

For example, given the following matrix:

[['F', 'A', 'C', 'I'],
 ['O', 'B', 'Q', 'P'],
 ['A', 'N', 'O', 'B'],
 ['M', 'A', 'S', 'S']]
and the target word 'FOAM', you should return true, since it's the leftmost column. Similarly, given the target word 'MASS', you should return true, since it's the last row.
"""


def search_word(matrix, target):
	row_len = len(matrix)
	col_len = len(matrix[0])

	for i in range(col_len):
		s = ''
		for j in range(row_len):
			s += matrix[j][i]
		
		if target in s:
			return True

	for i in range(row_len):
		if target in matrix[i]:
			return True
		
	return False
