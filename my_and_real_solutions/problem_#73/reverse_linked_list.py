"""
This problem was asked by Google.

Given the head of a singly linked list, reverse it in-place.
"""


class LinkedList:
    def __init__(self):
        self.head = None


class Node:
    def __init__(self, val):
        self.val = val
        self.next = None

    def __len__(self, head):
        count = 0
        while head is not None:
            count += 1
            head = head.next

        return count


def reverse_linked_list(head):
    ll_length = head.__len__(head)

    temp = head

    for i in range(ll_length-1):
        for j in range(i, ll_length-1):
            temp.val, temp.next.val = temp.next.val, temp.val
            temp = temp.next
        temp = head


ll = LinkedList()
ll.head = Node(2)
ll.head.next = Node(3)
ll.head.next.next = Node(1)
ll.head.next.next.next = Node(4)

print(ll.head.val, ll.head.next.val, ll.head.next.next.val, ll.head.next.next.next.val)

reverse_linked_list(ll.head)

print(ll.head.val, ll.head.next.val, ll.head.next.next.val, ll.head.next.next.next.val)
