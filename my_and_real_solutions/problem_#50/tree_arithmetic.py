"""
This problem was asked by Microsoft.

Suppose an arithmetic expression is given as a binary tree. Each leaf is an integer and each internal node is one of '+', '−', '∗', or '/'.

Given the root to such a tree, write a function to evaluate it.

For example, given the following tree:

    *
   / \
  +    +
 / \  / \
3  2  4  5
You should return 45, as it is (3 + 2) * (4 + 5)
"""


class Tree:
	def __init__(self, val):
		self.val = val
		self.right = None
		self.left = None


def tree_arithmetic_eval(root):
	if root is None:
		return ''

	return eval(str(tree_arithmetic_eval(root.left)) + str(root.val) + str(tree_arithmetic_eval(root.right)))

tree = Tree('*')
tree.left = Tree('+')
tree.right = Tree('+')
tree.left.left = Tree('-')
tree.left.right = Tree('-')
tree.right.left = Tree('*')
tree.right.right = Tree('+')
tree.left.left.left = Tree(3)
tree.left.left.right = Tree(2)
tree.left.right.left = Tree(5)
tree.left.right.right = Tree(2)
tree.right.left.left = Tree(1)
tree.right.left.right = Tree(7)
tree.right.right.left = Tree(3)
tree.right.right.right = Tree(4)
print(tree_arithmetic_eval(tree))