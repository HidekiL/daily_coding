import random 
import unittest

from num_search_in_mult_table import num_search as f 
from sol import multi_tables as _


class TestNumSearch(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f(6, 12)
		self.assertEqual(res, 4)

	def test_my_example(self):
		res = f(6, 1)
		self.assertEqual(res, 1)

		res = f(6, 36)
		self.assertEqual(res, 1)
		
	def test_empty(self):
		pass
		
	def test_small(self):
		res = f(1, 1)
		self.assertEqual(res, 1)

		res = f(1, 12)
		self.assertEqual(res, 0)

		res = f(2, 2)
		self.assertEqual(res, 2)

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		n = random.randint(1, 100)
		x = random.randint(1, n**2)

		self.assertEqual(f(n, x), _(n, x))


if __name__ == '__main__':
	unittest.main()