import unittest
from is_sum_of_two import add_up_to


class TestChallenge(unittest.TestCase):
	def setUp(self):
		pass

	def test_empty(self):
		res = add_up_to([], 3)
		self.assertFalse(res)

	def test_one_number(self):
		res = add_up_to([3], 3)
		self.assertFalse(res)

	def test_two_numbers(self):
		res = add_up_to([-3, 7], 4)
		self.assertTrue(res)

	def test_example(self):
		res = add_up_to([10, 15, 3, 7], 17)
		self.assertTrue(res)

	def test_weird(self):
		data = [21, 7, 13, -8, 23, 15, -1, 0, 1, 2071, 15]
		res1 = add_up_to(data, 0)
		res2 = add_up_to(data, -1)
		res3 = add_up_to(data, 4)

		self.assertTrue(res1)
		self.assertTrue(res2)
		self.assertFalse(res3)
		

if __name__ == '__main__':
	unittest.main()