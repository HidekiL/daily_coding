"""
Asked by Google.

Given a list of numbers and a number k, 
return whether any two numbers from the list add up to k.

For example, given [10, 15, 3, 7] and k of 17, 
return true since 10 + 7 is 17.

Bonus: Can you do this in one pass?
"""


def add_up_to(data, k):
	while data:
		curr_num = data.pop()
		rem = k - curr_num
		if rem in data:
			return True

	return False