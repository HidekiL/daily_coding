"""
This problem was asked by Airbnb.

We're given a hashmap associating each courseId key with a list of courseIds values, which represents that the
prerequisites of courseId are courseIds. Return a sorted ordering of courses such that we can finish all courses.

Return null if there is no such ordering.

For example, given {'CSC300': ['CSC100', 'CSC200'], 'CSC200': ['CSC100'], 'CSC100': []}, should return ['CSC100',
'CSC200', 'CSCS300']. """


def find_course_without_prerequisite(prerequists):
	for course, prerequisit in prerequists.items():
		if not prerequisit:
			return course


def remove_prerequisites(without_prerequisite, courses):
	for key, value in courses.items():
		if without_prerequisite in value:
			value.remove(without_prerequisite)

	del courses[without_prerequisite]


def finish_courses(course_prerequisites):
	ordered_courses = []
	num_of_courses = len(course_prerequisites)

	for i in range(num_of_courses):
		dont_need_prerequisite = find_course_without_prerequisite(course_prerequisites)

		if dont_need_prerequisite:
			ordered_courses.append(dont_need_prerequisite)
			remove_prerequisites(dont_need_prerequisite, course_prerequisites)
		else:
			return None

	return ordered_courses

