import random 
import unittest

from prerequisites import finish_courses as f 


class TestFinishCourses(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f({'CSC300': ['CSC100', 'CSC200'], 'CSC200': ['CSC100'], 'CSC100': []})
		self.assertEqual(res, ['CSC100', 'CSC200', 'CSC300'])

	def test_my_example(self):
		res = f({'CSC300': ['CSC100', 'CSC200'], 'CSC200': ['CSC300'], 'CSC100': []})
		self.assertEqual(res, None)
		
	def test_empty(self):
		pass
		
	def test_small(self):
		res = f({'A': ['B'], 'B': []})
		self.assertEqual(res, ['B', 'A'])

		res = f({'A': ['B'], 'B': ['A']})
		self.assertEqual(res, None)

	def test_weird(self):
		res = f({'A': [], 'B': [], 'C': [], 'D': []})
		self.assertEqual(['A', 'B', 'C', 'D'], res)

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()