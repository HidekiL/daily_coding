"""
This problem was asked by Dropbox.

Conway's Game of Life takes place on an infinite two-dimensional board of square cells. 
Each cell is either dead or alive, and at each tick, the following rules apply:

Any live cell with less than two live neighbours dies.
Any live cell with two or three live neighbours remains living.
Any live cell with more than three live neighbours dies.
Any dead cell with exactly three live neighbours becomes a live cell.
A cell neighbours another cell if it is horizontally, vertically, or diagonally adjacent.

Implement Conway's Game of Life. 
It should be able to be initialized with a starting list of live cell coordinates and 
the number of steps it should run for. Once initialized, 
it should print out the board state at each step. Since it's an infinite board, 
print out only the relevant coordinates, i.e. 
from the top-leftmost live cell to bottom-rightmost live cell.

You can represent a live cell with an asterisk (*) and a dead cell with a dot (.).
"""


# WRONG SOLUTION


import random
import time
import pprint


class Conway:
	def __init__(self, tick, height, width, init_cell_num, interval_between_ticks):
		self.interval_between_ticks = interval_between_ticks
		self.tick = tick
		self.height = height
		self.width = width
		self.init_cell_num = init_cell_num
		self.dead_cell = '.'
		self.live_cell = '*'
		self.board = self.create_new_random_board()

	def create_new_random_board(self):
		board = [['0' for i in range(self.width)] for j in range(self.height)]
		
		for i in range(self.init_cell_num):
			x = random.randint(2, self.height-3)
			y = random.randint(2, self.width-3)
			while board[x][y] == '*':
				x = random.randint(2, self.height-3)
				y = random.randint(2, self.width-3)
			board[x][y] = '*'
		
		return board

	def is_dead(self, x, y):
		return self.board[x][y] == '.'

	def is_alive(self, x, y):
		return self.board[x][y] == '*'

	def become_dead(self, x, y):
		self.board[x][y] = '.'

	def become_alive(self, x, y):
		self.board[x][y] = '*'

	def decr_tick(self):
		self.tick -= 1

	def print_current_board(self):
		pp = pprint.PrettyPrinter(indent=1)
		pp.pprint(self.board)
		print('\n\n\n')

	def alive_neighbours(self, x, y):
		"""
		Returns number of live cell neighbours
		"""
		count = 0

		if self.is_alive(x-1, y-1):
			count += 1
		if self.is_alive(x, y-1):
			count += 1
		if self.is_alive(x-1, y): 
			count += 1
		if self.is_alive(x, y+1):
			count += 1
		if self.is_alive(x+1, y):
			count += 1
		if self.is_alive(x+1, y-1):
			count += 1 
		if self.is_alive(x+1, y+1):
			count += 1
		if self.is_alive(x-1, y+1):
			count += 1

		return count

	def ticked(self):
		new_board = self.board.copy()
		for i in range(1, self.height-1):
			for j in range(1, self.width-1):
				if self.is_alive(i, j) and (2 > self.alive_neighbours(i, j) or self.alive_neighbours(i, j) > 3):
					new_board[i][j] = '.'
				elif self.is_dead(i, j) and self.alive_neighbours(i, j) == 3:
					new_board[i][j] = '*'
		self.board = new_board

	def play(self):
		self.print_current_board()
		wait_before_next_tick(self.interval_between_ticks)
		while self.tick > 0:
			self.ticked()
			self.print_current_board()
			self.decr_tick()
			wait_before_next_tick(self.interval_between_ticks)


def wait_before_next_tick(sec):
	time.sleep(sec)
