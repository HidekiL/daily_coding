"""
This problem was asked by Google.

Invert a binary tree.
"""


class Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

    def invert_tree(self):
        queue = [self]

        while queue:
            node = queue.pop(0)
            node.left, node.right = node.right, node.left

            if node.right:
                queue.append(node.right)

            if node.left:
                queue.append(node.left)


tree = Node('a')
tree.left = Node('b')
tree.right = Node('c')
tree.left.left = Node('d')
tree.right.left = Node('f')
tree.left.right = Node('e')

print(tree.val,
      tree.left.val,
      tree.right.val,
      tree.left.left.val,
      tree.left.right.val,
      tree.right.left.val
      )
tree.invert_tree()
print(tree.val,
      tree.left.val,
      tree.right.val,
      tree.left.right.val,
      tree.right.left.val,
      tree.right.right.val
      )
