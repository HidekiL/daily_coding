import unittest
import random

from sol import shortest_path as _ 
from min_steps import minimum_steps as f 


class TestMinimumSteps(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		board = [[False, False, False, False],
				[True, True, True, True],
				[False, False, False, False],
				[False, False, False, False]]
		res = f(board, (3, 0), (0, 0))
		self.assertEqual(res, 7)

	def test_other(self):
		pass

	def test_random(self):
		pass

		
if __name__ == '__main__':
	unittest.main()