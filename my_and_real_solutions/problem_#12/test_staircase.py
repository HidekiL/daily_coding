import unittest

from staircase import staircase


class TestStaircase(unittest.TestCase):
	def setUp(self):
		pass

	def test_examples(self):
		real_results = [1, 2, 3, 5, 8, 13]
		for i in range(1, 7):
			self.assertEqual(staircase(i, {1,2}), real_results[i-1])


if __name__ == '__main__':
	unittest.main()