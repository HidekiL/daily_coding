"""
This problem was asked by Amazon.

There exists a staircase with N steps, 
and you can climb up either 1 or 2 steps at a time.
Given N, write a function that returns 
the number of unique ways you can climb the staircase. 
The order of the steps matters.

For example, if N is 4,
then there are 5 unique ways:

1, 1, 1, 1
2, 1, 1
1, 2, 1
1, 1, 2
2, 2

What if, instead of being able to climb 1 or 2 steps at a time, 
you could climb any number from a set of positive integers X? 
For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time
"""


def staircase(n):
	if n == 0:
		return 0

	a, b = 1, 2

	for i in range(1, n):
		a = a + b
		a, b = b, a

	return a


"""
very slow, but correct
"""
def any_step_staircase(n, steps):
	climbings = [i for i in steps]
	new_climbings = []
	res = 0

	if climbings[0] == n:
		return 1

	if n in steps:
		res += 1

	while climbings[0] != n:
	
		for climbing in climbings:
			for step in steps:
				if step + climbing > n:
					break
				if step + climbing == n:
					res += 1
				new_climbings.append(step+climbing)

		climbings = new_climbings
		new_climbings = []

	return res