"""
This problem was asked by Microsoft.

Compute the running median of a sequence of numbers. 
That is, given a stream of numbers, print out the median of the list so far on each new element.

Recall that the median of an even-numbered list is the average of the two middle numbers.

For example, given the sequence [2, 1, 5, 7, 2, 0, 5], your algorithm should print out:

2
1.5
2
3.5
2
2
2
"""


import bisect 


def median(arr, num):
	return (arr[num//2-1]+arr[num//2]) / 2 if num % 2 == 0 else arr[num//2]


def medians_of_sequence(sequence):
	temp = []

	for ind, num in enumerate(sequence):
		if ind == 0:
			print(num)
			bisect.insort(temp, num)

		else:
			bisect.insort(temp, num)
			print(median(temp, ind+1)


medians_of_sequence([2, 1, 5, 7, 2, 0, 5])