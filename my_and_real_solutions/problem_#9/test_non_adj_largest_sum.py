import unittest
import random

from non_adj_largest_sum import largest_sum 


class TestLargestSum(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res1 = largest_sum([2, 4, 6, 2, 5])
		res2 = largest_sum([5, 1, 1, 5])
		self.assertEqual(res1, 13)
		self.assertEqual(res2, 10)

	def test_one_size(self):
		tst = random.randint(1, 1000)
		res = largest_sum([tst])
		self.assertEqual(res, tst)

	def test_my_examples(self):
		res1 = largest_sum([17, 3, 4, 1, 8, 13, 23, 15, 1, 1, 0])
		res2 = largest_sum([17, 3, 4, 1, 8, 23, 13, 15, 1, 1, 0])
		self.assertEqual(res1, 53)
		self.assertEqual(res2, 60)

	def test_other(self):
		res1 = largest_sum([i for i in range (1, 9)])
		res2 = largest_sum([i for i in range (1, 10)])
		self.assertEqual(res1, 20)
		self.assertEqual(res2, 25)


if __name__ == '__main__':
	unittest.main()