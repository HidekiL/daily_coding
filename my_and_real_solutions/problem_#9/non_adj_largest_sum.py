"""
Asked by Airbnb.

Given a list of integers, 
write a function that returns the largest sum of non-adjacent numbers. 
Numbers can be 0 or negative.

For example, [2, 4, 6, 2, 5] should return 13, 
since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, 
since we pick 5 and 5.

Follow-up: Can you do this in O(N) time and constant space?
"""


# WRONG SOLUTION


def largest_sums(lst, ind):
	lost1 = 0
	lost2 = 0
	max_sum = 0
	ln = len(lst)

	while ind < ln:
		if ind == ln - 1:
			max_sum += lst[ind]
			break

		elif ind == 0:
			lost1 = lst[ind + 1]
			lost2 = lst[ind] + lst[ind+2]
			

		elif ind == ln - 2:
			if lst[ind] > lst[-1]:
				max_sum += lst[ind]
			else:
				max_sum += lst[-1]
			break		

		elif ind == ln - 4:
			max_sum += max(lst[ind]+lst[ind+2], lst[ind+1]+lst[ind+3])
			break

		else:
			lost1 = lst[ind-1] + lst[ind+1]
			lost2 = lst[ind-1] + lst[ind] + lst[ind+2]
		
		if lost1 < lost2:
			max_sum += lst[ind]
			ind += 2
		else:
			max_sum += lst[ind+1]
			ind += 3

	return max_sum


def largest_sum(lst):
	sum1 = largest_sums(lst, 0)
	sum2 = largest_sums(lst, 1)
	return max(sum1, sum2)