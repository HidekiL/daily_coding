"""
This problem was asked by LinkedIn.

Determine whether a tree is a valid binary search tree.

A binary search tree is a tree with two children, left and right, and satisfies the constraint that the key in the
left child must be less than or equal to the root and the key in the right child must be greater than or equal to the
root. """


class Node:
	def __init__(self, val):
		self.val = val
		self.left = None
		self.right = None


def valid_binary_search_tree(root):
	queue = [root]

	while queue:
		node = queue.pop(0)

		left, right = node.left, node.right

		if left is not None:
			if left.val > node.val:
				return False
			else:
				queue.append(left)

		if right is not None:
			if right.val < node.val:
				return False
			else:
				queue.append(right)

	return True


tree = Node(5)
tree.left = Node(4)
tree.right = Node(9)
tree.right.left = Node(3)
tree.right.left.left = Node(1)
tree.right.left.right = Node(10)
tree.right.right = Node(15)
tree.right.right.left = Node(2)
tree.right.right.right = Node(19)
tree.left.left = Node(-2)
tree.left.right = Node(8)
tree.left.right.left = Node(7)
tree.left.right.right = Node(11)
tree.left.left.left = Node(-5)
tree.left.left.right = Node(20)

print(valid_binary_search_tree(tree))