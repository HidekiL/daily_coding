import random
import unittest

from regex import regex as f 
# from sol import matches as _ 


class TestRegex(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res1 = f('ra.', 'ray')
		res2 = f('ra.', 'raymond')
		self.assertEqual(res1, True)
		self.assertEqual(res2, False)

		res3 = f('.*at', 'chat')
		res4 = f('.*at', 'chats')
		self.assertEqual(res3, True)
		self.assertEqual(res4, False)

	def test_small(self):
		res1 = f('.', 'k')
		res2 = f('.', 'ok')
		self.assertEqual(res1, True)
		self.assertEqual(res2, False)

	def test_one_ast(self):
		res1 = f('*', 'slkjdflkajsdfljasfljsflkjaflj')
		res2 = f('*', 'niceToMeetYou')
		self.assertEqual(res1, True)
		self.assertEqual(res2, True)

	def test_weird_big(self):
		res1 = f('*ell.', 'hello')
		res2 = f('*ok*I.wanted*be*ive', 'man ok that I wanted to be alive')
		res3 = f('*...*testme', 'hellotestme')
		res4 = f('*ok*I.wanted*be*ive', 'man o that I wanted to be alive')
		res5 = f('i*.*wanted', 'idon"want it')
		res6 = f('i*.*wanted', 'idon"wanted')
		self.assertEqual(res1, True)
		self.assertEqual(res2, True)
		self.assertEqual(res3, True)
		self.assertEqual(res4, False)
		self.assertEqual(res5, False)
		self.assertEqual(res6, True)


if __name__ == '__main__':
	unittest.main()