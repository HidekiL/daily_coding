"""
This problem was asked by Facebook.

Implement regular expression matching with the following special characters:

. (period) which matches any single character
* (asterisk) which matches zero or more of the preceding element
That is, implement a function that takes in a string and a valid regular expression and 
returns whether or not the string matches the regular expression.

For example, given the regular expression "ra." 
and the string "ray", your function should return true. 
The same regular expression on the string "raymond" should return false.

Given the regular expression ".*at" and the string "chat", 
your function should return true. 
The same regular expression on the string "chats" should return false.
"""


def regex(reg, s):
	
	while s:
		try:
			if s[0] == reg[0] or reg[0] == '.':
				s = s[1:]
				reg = reg[1:]

			elif reg[0] == '*':
				if len(reg) == 1:
					return True

				while reg[0] in ['.', '*']:
					reg = reg[1:]


				while s[0] != reg[0]:
					s = s[1:]
			else:
				return False
		except:
			return False

	return True
