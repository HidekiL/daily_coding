import random 
import unittest

from coloring_graph import coloring_graph as f 


class TestColoringGraph(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		pass

	def test_my_example(self):
		res = f([[0, 1, 0, 1, 0, 1],
				 [1, 0, 1, 1, 0, 0],
				 [0, 1, 0, 1, 1, 0],
				 [1, 1, 1, 0, 0, 1],
				 [0, 0, 1, 0, 0, 1],
				 [1, 0, 0, 1, 1, 0]], 3)
		self.assertEqual(res, False)

		res = f([[0, 1, 0, 1, 0, 1],
				 [1, 0, 1, 1, 0, 0],
				 [0, 1, 0, 1, 1, 0],
				 [1, 1, 1, 0, 0, 1],
				 [0, 0, 1, 0, 0, 1],
				 [1, 0, 0, 1, 1, 0]], 4)
		self.assertEqual(res, True)

		res = f([[0, 1, 0, 1, 0, 1],
				 [1, 0, 1, 1, 0, 0],
				 [0, 1, 0, 1, 1, 0],
				 [1, 1, 1, 0, 0, 0],
				 [0, 0, 1, 0, 0, 1],
				 [1, 0, 0, 0, 1, 0]], 3)
		self.assertEqual(res, True)

	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		pass


if __name__ == '__main__':
	unittest.main()