"""
This problem was asked by Google.

Given an undirected graph represented as an adjacency matrix and an integer k, 
write a function to determine whether each vertex in the graph can be colored such that 
no two adjacent vertices share the same color using at most k colors.
"""


def get_adjacent_vertices_colors(vertex_colrs, row):
	colrs = []
	for ind, el in enumerate(row):
		if el == 1:
			if vertex_colrs[ind+1] and vertex_colrs[ind+1] not in colrs:
				colrs.append(vertex_colrs[ind+1])

	return sorted(colrs) + [0]


def coloring_graph(matrix, k):
	colors = [i+1 for i in range(k)] # initialize different colors with different numbers
	vertex_colors = {} 

	for i in range(len(matrix)):
		vertex_colors[i+1] = None # initialize vertex colors with None

	for row_ind, row in enumerate(matrix):
		neighbour_colors = get_adjacent_vertices_colors(vertex_colors, row)

		if neighbour_colors[:k] == colors:
			return False

		# set first non occuring color from adjacent vertices to current vertex
		for color, neighbour_color in zip(colors, neighbour_colors):
			if color != neighbour_color:
				vertex_colors[row_ind+1] = color
				break

	return True
