"""
This problem was asked by Amazon.

Given a matrix of 1s and 0s, return the number of "islands" in the matrix. A 1 represents land and 0 represents
water, so an island is a group of 1s that are neighboring whose perimeter is surrounded by water.

For example, this matrix has 4 islands.

1 0 0 0 0
0 0 1 1 0
0 1 1 0 0
0 0 0 0 0
1 1 0 0 1
1 1 0 0 1
"""


def island_count(matrix):
    seen = set()
    current_islands = []
    count = 0

    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if matrix[i][j] == 1 and (i, j) not in seen:
                current_islands.append((i, j))
                seen.add((i, j))
                count += 1

                while current_islands:
                    x, y = current_islands.pop(0)

                    if x - 1 >= 0 and matrix[x - 1][y] == 1 and (x - 1, y) not in seen:
                        current_islands.append((x - 1, y))
                        seen.add((x - 1, y))
                    if x + 1 < len(matrix) and matrix[x + 1][y] == 1 and (x + 1, y) not in seen:
                        current_islands.append((x + 1, y))
                        seen.add((x + 1, y))
                    if y - 1 >= 0 and matrix[x][y - 1] == 1 and (x, y - 1) not in seen:
                        current_islands.append((x, y - 1))
                        seen.add((x, y - 1))
                    if y + 1 < len(matrix[0]) and matrix[x][y + 1] == 1 and (x, y + 1) not in seen:
                        current_islands.append((x, y + 1))
                        seen.add((x, y + 1))

    return count
