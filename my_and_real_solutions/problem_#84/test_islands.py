import random 
import unittest

from islands import island_count as f
from sol import num_islands as res_f


class TestIslandCount(unittest.TestCase):
	def setUp(self):
		pass

	def test_example(self):
		res = f([[1, 0, 0, 0, 0],
              [0, 0, 1, 1, 0],
              [0, 1, 1, 0, 0],
              [0, 0, 0, 0, 0],
              [1, 1, 0, 0, 1],
              [1, 1, 0, 0, 1]])
		self.assertEqual(res, 4)

	def test_my_example(self):
		pass
		
	def test_empty(self):
		pass
		
	def test_small(self):
		pass

	def test_weird(self):
		pass

	def test_big(self):
		pass

	def test_random(self):
		inp = [[random.randint(0, 1) for _ in range(100)] for _ in range(100)]
		self.assertEqual(f(inp), res_f(inp))


if __name__ == '__main__':
	unittest.main()