import random 
import unittest

from sol import build_houses as f
from houses_and_colors import minimum_cost


class TestMinimumCost(unittest.TestCase):
	def setUp(self):
		pass

	def test_my_example(self):
		inp = [[2, 3, 1], 
			   [3, 7, 2], 
			   [4, 1, 2], 
			   [5, 3, 2]]
		res = minimum_cost(inp)
		self.assertEqual(res, 7)

	def test_random(self):
		inp = []
		for i in range(10):
			x = [random.randint(1, 10) for _ in range(6)]
			inp.append(x)

		self.assertEqual(minimum_cost(inp), f(inp))


if __name__ == '__main__':
	unittest.main()