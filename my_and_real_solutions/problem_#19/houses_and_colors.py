"""
This problem was asked by Facebook.

A builder is looking to build a row of N houses 
that can be of K different colors. 
He has a goal of minimizing cost while ensuring 
that no two neighboring houses are of the same color.

Given an N by K matrix where the nth row and kth column represents 
the cost to build the nth house with kth color, 
return the minimum cost which achieves this goal.
"""


def minimum_cost(costs):
	min_costs = costs[0]
	curr_min_costs = []

	for i in range(1, len(costs)):
		for ind, num in enumerate(costs[i]):
			curr_min_costs.append(num + min(min_costs[:ind]+min_costs[ind+1:]))
		min_costs = curr_min_costs
		curr_min_costs = []

	return min(min_costs)
