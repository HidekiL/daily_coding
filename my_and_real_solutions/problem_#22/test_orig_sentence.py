import random
import unittest

from orig_sentence import orig_sentence as f
# from sol import orig_sentence as _


class TestOrigSentence(unittest.TestCase):
	def setUp(self):
		pass

	def test_examples(self):
		res1 = f(['quick', 'brown', 'the', 'fox'], "thequickbrownfox")
		res2 = f(['bed', 'bath', 'bedbath', 'and', 'beyond'], "bedbathandbeyond")
		self.assertEqual(['the', 'quick', 'brown', 'fox'], res1)
		self.assertEqual(['bed', 'bath', 'and', 'beyond'] or ['bedbath', 'and', 'beyond'], res2)

	def test_other(self):
		res = f(['the', 'theremin'], "theremin")
		self.assertEqual(res , ['theremin'])

if __name__ == '__main__':
	unittest.main()