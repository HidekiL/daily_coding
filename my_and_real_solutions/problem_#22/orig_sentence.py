"""
This problem was asked by Microsoft.

Given a dictionary of words and a string made up of those words (no spaces), 
return the original sentence in a list. If there is more than one possible reconstruction, 
return any of them. If there is no possible reconstruction, then return null.

For example, given the set of words 'quick', 'brown', 'the', 'fox', 
and the string "thequickbrownfox", 
you should return ['the', 'quick', 'brown', 'fox'].

Given the set of words 'bed', 'bath', 'bedbath', 'and', 'beyond', and 
the string "bedbathandbeyond", return either ['bed', 'bath', 'and', 'beyond] 
or ['bedbath', 'and', 'beyond'].
"""

# Wrong solution

def orig_sentence(words, strng):
	res = []

	start_ind = 0
	for i in range(len(strng)):
		sub_str = strng[start_ind:i+1]

		if sub_str in words:
			res.append(sub_str)
			start_ind = i+1

	return res
